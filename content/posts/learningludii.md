+++
title = "🎲 Learning Ludii"
date = 2021-04-20
description="Learning the Ludii General Game System language."

[taxonomies]
categories = ["Guide"]
tags = ["Challenge", "Ludii"]
+++

# Introduction
## What is Ludii?
Ludii is a "general game system designed to play, evaluate and design a wide range of games, including board games, card games, dice games, mathematical games, and so on." (taken from [Ludii's website](https://ludii.games/)).

Ludii games (in .lud file format) are usually loaded in the [Ludii Player](https://ludii.games/download.php), which compiles the game with Java and gives an interface to play with, with also AIs to play against. This won't stop to amaze me: from a short game description you can immediately get a fully functioning environment to play in, against an AI that has never seen the game before but might still beat you.

> You can install the Ludii player on Linux by running "`curl https://fabiobarbero.eu/scripts/ludii.sh | sh`" in a shell terminal. [^warning]

I am lucky enough to have Cameron Browne, the creator of Ludii, as a supervisor for my bachelor thesis. His recent talk at the [XXIII board game studies colloquium](https://bgsparis2020.com/) made me realize how truly revolutionary the Ludii language is: a "game" in Ludii is described uniquely by "ludemes", giving hence a solid mathematical definition of a Ludeme.


## What is a _ludeme_?
Much like a _meme_, a ludeme is something that spreads by means of imitation from person to person within a culture and often carries symbolic meaning (from [Merriam-webster dictionary](https://www.merriam-webster.com/dictionary/meme)). 
_Ludemes_ apply to games in general: this can be rules such as hopping over a piece to capture it, reaching the opposite side of the board to win, or moving forward.

Cameron Browne describes a ludeme as:
- a discrete unit of information (atomic or compound)
- can be transferred between games
- changes the function of a game

In ludii, everything is a ludeme.


# Learning Ludii
As of when I'm writing this blog the only documentation that can be found for learning the Ludii language is:
- [Ludii's readthedocs](https://ludiitutorials.readthedocs.io/en/latest/lud_format_basics.html)
- [Ludii Game Logic Guide](https://ludii.games/downloads/LudiiGameLogicGuide.pdf)
- [Ludii Language Reference](https://ludii.games/downloads/LudiiLanguageReference.pdf)

I am proud to say that I have read almost all of these three documents, and hope to be able to save some of your time by writing this guide. If you would like to read the original documentation, I recommend doing it in the order listed above.

## What does Ludii look like?
The "hello world" of board games is often considered to be the game Tic-Tac-Toe, which I am assuming everyone is familiar with. Here is what Tic-Tac-Toe looks like in Ludii:
```lisp
(game "Tic-Tac-Toe"  
    (players 2)  
    (equipment { 
        (board (square 3)) 
        (piece "Disc" P1) 
        (piece "Cross" P2) 
    })  
    (rules 
        (play (move Add (to (sites Empty))))
        (end (if (is Line 3) (result Mover Win)))
    )
)
```
If you're familiar with Lisp, then this should be fairly easy to read. Ludemes are encapsulated in parentheses. If you're not, don't worry. I also had very little experience with Lisp when I started learning Ludii, knowing mainly Java and Python. 
Intuitively, the first word in the parentheses is the function, and the remaining terms are arguments. A list of all functions can be found in the [Ludii Language Reference](https://ludii.games/downloads/LudiiLanguageReference.pdf).

So the above code can be read as following: we define a `game` called "Tic-Tac-Toe". In this game, there are 2 `players`, and the `equipment` is: a 3x3 `square` `board`, `Disc` pieces for Player 1 and `Cross` pieces for Player 2. The `rules` of the game are: keep adding pieces to empty sites, until a `line` of 3 in any direction is made, which results in the Mover winning.

## How to learn ludii
In this post I won't go through every single ludeme describing how they work in practice. You can find that in the [Ludii Language Reference](https://ludii.games/downloads/LudiiLanguageReference.pdf). 
I would just like to give a few tips on how to learn it, based on my own experience.

### Read, read, read
The first thing I did to get familiar with the syntax is to read a lot of implementations of games I was already familiar with.
A full list of games can be found in the [Ludii Player](https://ludii.games/download.php) or [on the ludii library](https://ludii.games/library.php).

Here's a list of games you might be familiar with:
- [Tic-Tac-Toe](https://ludii.games/lud/games/Tic-Tac-Toe.lud)
- [Five Men's Morris](https://ludii.games/lud/games/Five%20Men's%20Morris.lud)
- [Chinese Checkers](https://ludii.games/lud/games/Chinese%20Checkers.lud)
- [Dama](https://ludii.games/lud/games/Dama%20(Italy).lud)
- [Chess](https://ludii.games/lud/games/Chess.lud)
- [Backgammon](https://ludii.games/lud/games/Backgammon.lud)
- [Go](https://ludii.games/lud/games/Go.lud)
- [Rock-Paper-Scissor](https://ludii.games/lud/games/Rock-Paper-Scissors.lud)

Don't worry if you sometimes don't understand what is going on. This is just to get used to the syntax and getting used to reading Ludii code - useful for debugging. You can also ignore the `(define)` commands and start reading from `game`.

### Get a clear structure of how ludii works
The [Ludii Game Logic Guide](https://ludii.games/downloads/LudiiGameLogicGuide.pdf) is in my opinion the best document to get a clear idea of how the language works.

The main components of a ludii games are:
- players
- equipment
- rules
    - start (optional)
    - play
    - end

### Practice bit by bit
Unfortunately for you, Stack Overflow does not have anything for ludii yet. This means that you'll have to learn the hard way, by debugging every single error you get.

#### Board generation
It is important to start experimenting with each of the different components listed above. For example, you might first want to get familiar with the board generation. In order for the game to compile, you also require the rest of the rules. I usually use these "dummy" rules to check the board generation
```lisp
(game "test"
    (players 2)
    (equipment {
        // your code here
    })
    (rules
        (play (forEach Piece))
        (end (if (no Moves Next) (result Mover Win)))
    )
)
```


**Exercise!** Try to generate the following boards. You can **click** on the image to reveal the solution
<script src="/js/hide.js"></script>
<table>
    <tr>
        <td onclick=hide(b1)><img src="/images/ludii/learnludii/Adugo.png" alt="Ludii Adugo board"></td>
        <td id="b1" style="opacity:0">
            <pre style="background-color:#2b303b;"><code class="language-lisp" data-lang="lisp"><span style="color:#abb2bf;">(board
    (</span><span style="color:#5ebfcc;">merge 
        </span><span style="color:#abb2bf;">(shift </span><span style="color:#db9d63;">0 2 </span><span style="color:#abb2bf;">(square </span><span style="color:#db9d63;">5</span><span style="color:#abb2bf;"> diagonals:Alternating)) 
        (wedge </span><span style="color:#db9d63;">3</span><span style="color:#abb2bf;">)
    ) 
use:Vertex)
</span></code></pre>
        </td>
        <td>Adugo</td>
    </tr>
    <tr>
        <td onclick=hide(b2)><img src="/images/ludii/learnludii/Asalto.png" alt="Ludii Asalto board"></td>
        <td id="b2" style="opacity:0">
            <pre style="background-color:#2b303b;"><code class="language-lisp" data-lang="lisp"><span style="color:#abb2bf;">(board
    (</span><span style="color:#5ebfcc;">merge
        </span><span style="color:#abb2bf;">(shift </span><span style="color:#db9d63;">0 2 </span><span style="color:#abb2bf;">(rectangle </span><span style="color:#db9d63;">3 7</span><span style="color:#abb2bf;"> diagonals:Alternating))
        (shift </span><span style="color:#db9d63;">2 0 </span><span style="color:#abb2bf;">(rectangle </span><span style="color:#db9d63;">7 3</span><span style="color:#abb2bf;"> diagonals:Alternating))
    )
    use:Vertex
)
</span></code></pre>
        </td>
        <td>Asalto</td>
    </tr>
    <tr>
        <td onclick=hide(b3)><img src="/images/ludii/learnludii/Kaooa.png" alt="Ludii Kaooa board"></td>
        <td id="b3" style="opacity:0">
            <pre style="background-color:#2b303b;"><code class="language-lisp" data-lang="lisp"><span style="color:#abb2bf;">(board 
    (splitCrossings (regular Star </span><span style="color:#db9d63;">5</span><span style="color:#abb2bf;">))
    use:Vertex
)
</span></code></pre>
        </td>
        <td>Kaooa</td>
    </tr>
</table>

#### Piece placement
Once you're comfortable with board generation, you can start playing around with the initial placement of the pieces.

**Exercise!** Try to generate the following piece placements. You can **click** on the image to reveal the solution
<table>
    <tr>
        <td onclick=hide(p1)><img src="/images/ludii/learnludii/ThreeMusketeers.png" alt="Ludii Three Musketeers board"></td>
        <td id="p1" style="opacity:0">
            <pre style="background-color:#2b303b;"><code class="language-lisp" data-lang="lisp"><span style="color:#abb2bf;">(start {
    (place </span><span style="color:#9acc76;">"Musketeer1"</span><span style="color:#abb2bf;"> {</span><span style="color:#9acc76;">"A1" "C3" "E5"</span><span style="color:#abb2bf;">})
    (place </span><span style="color:#9acc76;">"Enemy2" </span><span style="color:#abb2bf;">(difference 
        (sites Board) 
        (sites {</span><span style="color:#9acc76;">"A1" "C3" "E5"</span><span style="color:#abb2bf;">})
    ))
})
</span></code></pre>
        </td>
        <td>Three Musketeers</td>
    </tr>
</table>


#### Rules and end condition
I might add an interactive section here in the future, but I suggest looking at the [Ludii Language Reference](https://ludii.games/downloads/LudiiLanguageReference.pdf) for a full list of moves.

### Copy!
Try to write as many games as possible, by copying from games that already exist in Ludii. Before writing the game, think of similar games and look at how they have been implemented. It often gives great help and is a valuable learning experience. It is also how game designers often (subconsciously) design new games!
Some great games to start learning:
- [Yavalath](https://ludii.games/lud/games/Yavalath.lud)
- [Three Musketeers](https://ludii.games/lud/games/Three%20Musketeers.lud) (proudly written in ludii by me!)

### Create!
Ludii has a [list of games](https://ludii.games/wishlist.php) that have not yet been implemented. 

### Ask in the Ludii forum!
If you're really stuck on something and cannot find anything in the guides, you can always ask [the Ludii forum](https://ludii.games/forums/index.php)! 

Sometimes, what you're trying to do just isn't possible in the current version of Ludii! The ludii team is very friendly and competent and has always helped me with useful advice.

Here is [my profile](https://ludii.games/forums/member.php?action=profile&uid=21) on the Ludii Forum, if you would like to see what posts I made.

## Conclusion
Learning Ludii has been a fun and valuable to me. I hope that I have given you a few resources and tips to learn ludii by yourself. The key message is to try to copy/replicate as much as possible from existing games. 

I'd like to end this post with a quote from Cameron (during one of our meetings):

> Writing/creating a game can be a game by itself

[^warning]: It is generally NOT a good idea to pipe the content of a website into bash, so you should ideally first download the script locally, check what it does, and then run it. If you do not understand some of the code you can use https://explainshell.com/.
+++
title = "Kobo | Quick access"
date = 2021-07-25
description="Quick links for Kobo"

extra.sitemap_ignore = true
+++

# Kobo | quick access
- [The Hedgehog and the fox](/data/hedgehog_fox.pdf)
- [Vegan cookbook](/data/ricette-vegan-home.epub)
- [Vegan ebooks](https://www.vegandiscoverytour.it/utente/raccolta/materiali/)
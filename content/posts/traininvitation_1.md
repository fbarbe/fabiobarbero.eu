+++

title = "🇪🇺 An invitation to travel by train in Europe"

date = 2025-01-28

description=""

[taxonomies]

categories = ["Random"]

tags = ["An invitation to travel by train", "Train of thoughts", "Recommended Posts"]

+++

It was a late Saturday afternoon. Gaelle, her sister Quiteri and I were on a train from Püspökladány, Hungary, to Oradea, Romania. I met Gaelle around 1 year ago, at [my favourite bar in Montreal](https://www.turbohaus.ca/). It was quite funny to meet again on a small train, in Hungary.

Gaelle and Quiterie have a crazy goal in mind: to reach Japan from France, without taking the plane. Being able to work [on my master thesis remotely](https://fabiobarbero.eu/tags/on-trains/), I, of course, decided to join them for the first part across Romania.

It was fully dark outside. We had left Vienna earlier that morning, but our train to Püspökladány left with more than 1-hour delay. The train was initially there on time, but we were told to get off the train as soon as we got in and wait at the station. We bonded with some fellow passengers, such as an Austrian guy who had just come back from Mexico and was now heading to a silent meditation retreat in Budapest, and a [French movie director](https://juliebiro.eu/) going to shoot a film.

Having missed our connections, we hopped on a train that didn't appear on our schedule but that a train conductor pointed to us (in Hungarian).

The three of us combined could speak French, English, Italian, German, Spanish, Mandarin Chinese, Portuguese, and some notions of Russian, Dutch and Danish. But we were in Hungary. The Hungarian language has no common roots with any of those languages. Nothing. Only unknown sounds.

Attempting to have a conversation with fellow passengers was therefore difficult. The train was neither full nor empty, and most passengers seemed to be amused by our presence. A man, who could only speak Hungarian, offered me to drink from his bottle of (homemade?) alcohol and beer, which I kindly refused (had it been kombucha, I would have gladly accepted).

The train suddenly stops. The train inspector tries to tell us something. No way we understand anything she says. After a while, we find a loophole: another passenger could speak both Hungarian and Romanian, they would then translate it to a woman who spoke Romanian and German, and I would translate back to French to my friends. All this to say "the train doesn't work, we don't know why".

We eventually managed to get to Romania and spent some amazing days with the kind-hearted friends of Quiteri visiting Romania by car. We saw spooky castles, ate delicious soups and aubergine spreads.

My travel back to Central Europe was done on a night train from Bucharest to Vienna (18h), where I was lucky to share my sleep cabin with [Hendrik](https://camps.wwf-jugend.de/ueber-uns/team), with whom I talked about life in German and lived some intense adventures trying to help a disabled person to not get thrown off the train for not having a ticket.

This is one of the many experiences I've had since starting to write my thesis, which made me realize that I want to keep track of some of the things that are happening, and perhaps even share them with the world. This post may therefore be the start of a new series of travel adventures!

## Where have I been the past few months?

![](/images/traintravel/Thesis_trips_part1.png)

Having travelled almost exclusively with two Interrail passes, I managed to retrieve the data of my trips, with train stops and times. Last night, I decided to finally start learning the open source [QGIS](https://qgis.org/) software, and made my first map ever of my train travels. If you have any advice on how to improve my maps, please let me know!

There are some minor mistakes on the map (trips missing, slightly off stop location), but I'm going to go ahead and call this "lazy [differential privacy](https://en.wikipedia.org/wiki/Differential_privacy)".
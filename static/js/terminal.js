var prompt = $('.prompt');
var terminalResponse = $('.response');

function runScripts(data, pos) {
  document.getElementById("overviewBtn").disabled = true;
  script = data[pos];
  switch (script.action) {
    case 'type':
      // cleanup for next execution
      prompt.removeData();
      terminalResponse.removeData();
      $('.typed-cursor').text('');
      prompt_text = prompt.html();
      prompt.html(script.input);
      if (script.immediate) {
        console.log(script.output[0]);
        terminalResponse.html(script.output[0]);
        updateTerminal(script.input, terminalResponse.html(), pos, data);
      } else {
        terminalResponse.typed({
          strings: script.output,
          typeSpeed: -script.output[0].length,
          callback: function () {
            updateTerminal(script.input, terminalResponse.html(), pos, data);
          }
        });
      }

      break;
    case 'clear':
      prompt.removeData();
      prompt_text = prompt.html();
      $('.history').html('');
      prompt.html(prompt_text);
      document.getElementById("input").focus();
      updateTerminal(null, null, pos, data);
  }
}

function updateTerminal(input, response, pos, data) {
  if (input != null) {
    var history = $('.history').html();
    history = history ? [history] : [];
    history.push('$ ' + input);
    history.push(response);
    prompt.html(prompt_text);
    document.getElementById("input").focus();
    $('.history').html(history.join('<br>'));
    terminalResponse.html("");
  }
  // scroll to bottom of screen
  // $('section.terminal').scrollTop($('section.terminal').height());
  // Run next script
  pos++;
  if (pos < data.length) {
    setTimeout(function () {
      runScripts(data, pos);
    }, script.postDelay || 1000);
  }
  document.getElementById("overviewBtn").disabled = false;
}
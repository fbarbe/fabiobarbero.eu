+++
title = "💮 Winning a battle"
date = 2022-12-17
description="Diary of a short victory"

[taxonomies]
categories = ["Random"]
tags = ["Ethics", "Recommended Posts"]
+++

Today I won a battle. Not a big one or a significant one, but one that, I feel, is worth telling a story about.

As a part of their Data Science Master at Maastricht University, Master students can decide to work on a project with a company. Some companies send Project Proposals to the faculty, which publishes them to an intranet as well as printing them after being approved.

When looking at these proposals, I found two of them being related to using Artificial Intelligence for the military. One of it was on generating web content for the military from a briefing, and the other one was titled “Optimizing Military Behaviour”. Universities collaborating with the military [isn't too uncommon](https://www.jhunewsletter.com/article/2022/11/teach-in-addresses-universitys-complicity-in-war-and-climate-disaster), but after discussing it with friends from university, we all didn't quite like the fact that our department was promoting and helping the military. To make sure this wasn't a mistake, I went to talk the staff that published this proposal and expressed my ethical concern; she replied with a big smile, “well, the Board of Examiners approved this”. After a short awkward silence and a nervous laugh, she said, “this is not the answer you wanted, right?”. Yeah. The staff at my university is all really nice and kind, and they are always really open to talk about issues we have about the program or other. For once, I felt as if the university didn't really care about my feedback, and hid behind a bureaucratic wall.

So, I decided to strike back. I wrote a highly satirical document in the style of one of the military ones, with [GPT-3](https://arxiv.org/abs/2005.14165) as a co-author, and sent it to a bunch of friends. This document got printed and was hanged right next to the other ones, with bets of how long it would take for it to be noticed and taken down.

<img alt="The fake proposal" style="width:100%;max-width:500px" src="/images/winningabattle/fake.jpeg"><br />
<img alt="Board before" style="width:100%;max-width:500px" src="/images/winningabattle/before.jpeg">

Surprisingly, it didn't take long for them to notice it. Two days later, the poster was taken down from the board. Bummer. So, why is this post titled “Winning a battle” you might ask? When looking at the board a second time, I realized that my fake proposal wasn't the only one taken down. The other two military proposals had been taken down as well!

<img alt="Board after" style="width:100%;max-width:500px" src="/images/winningabattle/after.jpeg">

They are still available on the intranet, and there's a chance they will be put back up later on. But I was happy to see that my action that started as a joke had a concrete impact on things, for however small it might be.

I encourage anyone to do the same. I believe that it is important for anyone to have their voice heard, even when it comes to touchy subjects.
This post doesn't discuss the actual ethical issues of universities collaborating with the military, and I am certain that they are valid arguments as to why that is completely normal or even beneficial. But topics that raise ethical questions should be discussed, and a good explanation should be provided as to why when explicitly asked. I, personally, would feel uncomfortable working on, or having friends working on, optimizing the tools to kill humans or possibly generate propaganda online. The same applies to fossil fuel companies, or other topics I consider unethical.

Note: I have blurred the original proposal for fear of it being used against me, since I would be disclosing what, I believe, is non-public information.
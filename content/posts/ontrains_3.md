+++
title = "🧑‍🔬 Homo railpiens: what's it like to interact with train experts?"
date = 2024-12-22
description="🚉 On Trains: Post #3"

[taxonomies]
categories = ["Random"]
tags = ["On Trains"]
+++

## Introduction
4 month ago, before starting [my thesis "on trains"](/posts/ontrains-0), I had never worked in the field of "trains".
I am used to working in completely different environment. I have worked at a [well structured company, where developers text me on Discord while being in the same room](/posts/odooexperience), [unorganised startups where the founders work full time on other projects](/posts/roughstart), [academic environments in which you won't talk about anything else other than your research](/portfolio), ...

Each one of these environments has very different vibes, and you can expect people to communicate, think, and act differently. Busy university professors will most likely not enjoy a chat about the weather, whereas small talk is a very important part of startup environments.

This post summarises my very short experience interacting with actors in the railway field, from academics, policy makers, lobbyists and hobbyists.
Communication with humans, both online and in person, is essential for fields with so many actors at play.

I am not an anthropologist. This is not an attempt to do a cultural study. It's just a fun collection of unique and quirky behaviours I noticed in these environments.

## Reaching out
Being new to the field, I reached out to virtually any organisation, actor, or hobbyist I could find. It was often either hobby projects like [chronotrains.com](https://www.chronotrains.com/), [reports and rail studies](https://op.europa.eu/en/publication-detail/-/publication/c33890a8-a4db-11ec-83e1-01aa75ed71a1/language-en#) by large organisations, or software project maintainers.

I have reached out to around 40 distinct parties. That is indeed quite a lot!
Here's a chart of the people I've reached out to. The response rate is pretty high, at around 40%.

![](/images/thesisontrains/percentage_reach_out.png)

*(how did I keep track of this?) As written [in my first post](/posts/ontrains-0), I am writing down an entry for every day I work with everything I do. I ran a small LLM on my computer (on my CPU!) asking it to return a CSV of every person I contacted. The response was far from accurate and took a lot of prompt engineering and manual selection, but it was a fun process. I can just say it's inaccurate because of "differential privacy" lol.*

You may notice that does not include a lot of people from Academia. That is mostly because all _practical_ studies with _large scale, current data_ I found were carried out by institutions such as the European Commission.
## 4th ERA Multimodal Conference
Last week, on the 10th of December 2024, I attended the 4th European Union Agency for Railways (ERA) Multimodal conference. I had been invited by an automated message by ERA, having previously reached out to them for feedback on my thesis.

I found myself in a room filled with white men, averaging around 45 years old, all in a suit and tie. I, a younger white man, had come in my sweater, and felt slightly uncomfortable in this fancy environment. The room was filled with "important" people: parliamentarians, heads of different rail organisations, ministers, ... They all seemed to like the formal attitude, in fact many of them described the conference as "small and informal". Everyone spoke English with a mild European accent, and a lot of people started talking in a different language when reading the name on the badge of their interlocutor.

Networking was fairly easy: everyone seemed to enjoy the breaks a lot more than the talks and discussion panels, where most of the room was on their phone. I've exchanged contacts with many people that could potentially help me with my thesis.
The networking was made particularly easy by the abundant amount of food available between each break, with a lunch, multiple snacks and dinner.

My favourite memory of the event will remain the delicious raw vegetables with hummus that were served as a snack.

One person, within 10 years of my age, disliked that I referred to them with the informal French "tu". This was the first time it happened to me in a very long time.

I came there with little to no expectations, and was really surprised to see how close this environment resembled the one for politics. The talks themselves didn't really interest me, as they were exclusively about freight trains and regulations for them.

Most of my interactions were friendly but distant. Everyone expressed being upset about the current state of train data, with no "visionary" ideas on how to fix it. Chatting around, I also learned that a lot of those people came to Brussels, depressingly, by plane. What a shame!

Despite my comment being slightly critical, please invite me again ERA :). Your hummus was really good. More seriously, it was nice to see that these people were open for conversation, and some of them really had a passion for sustainable travel and "the European dream". Being able to have direct contact with decision makers is not something to be taken for granted.
## Hobbyists
Most of the people I reached out to were related to projects I found that mostly seemed "hobby" projects. Everyone was always very responsive, and was excited to see what I would do with my research.

I have not so far encountered many "idealists". It's mostly people complaining about the state of train data, regulations, or connections. Perhaps it's because I agree with everything that is said, but it feels like everyone is stuck in a limbo of suboptimality, where no one really feels they have full control of things. It's nice to see that people are active and start so many projects.

A particularly nice discovery was the activity of certain people on Mastodon. In particular, this week's shoutout goes to [Jon Worth](https://gruene.social/@jon) and [Pieter Colpaert](https://mastodon.social/@pietercolpaert). I yet have to fully explore all their cool projects, which I will surely do, but the few I've read reflected a wholesome enthusiasm.

## Closing thoughts
It's been overall really nice to be interacting with people in fields related to trains, in particular with hobbyists. The state of public data seems to be unknown to most of the people I talked to. Don't get me started on the fact that UIC railway data is not openly available. Someone said half-jokingly that I should build 15m of rail tracks in my backyard, just to be officially a "railway company" and have access to UIC data. 

This blogpost was particularly rushed, because I want to spend time with my family during the winter break.
I was very happy to see that [my last blogpost](/posts/ontrains-2) made it to [weeklyOSM](https://weeklyosm.eu/archives/17641) and was read by a good amount of people. It also sparked some discussion on a telegram Italian group chat about OSM, about fixing the issue with UIC codes in OSM.

In general, it was really nice to have some nice plots to show around. It certainly gave me some credibility, and made people want to know more.
It makes me a little nervous to know cool people like Jon and Pieter might read my blog, and makes me want to somehow "keep the level" of interesting posts like last week's. I'm happy with the decision to keep posting whatever is on my mind, and mostly write for myself. Occasionally, some stuff will be more suited for the general public and I might share it more.

*Note: no text in this document has been generated or rewritten by a Large Language Model.*
+++
title = "🌍 Green guide Maastricht"
date = 2019-02-02

[taxonomies]
categories = ["Guide"]
tags = ["Maastricht", "Environment"]
+++
_I found this old document I wrote years ago, thought I might put it here. Content is highly outdated. For a more updated soustainable guide, check out the [Sustainable Guide Maastricht](https://sustainabilityguidemaastricht.nl)_

# Short guide on how to be a green (and healthy) student in Maastricht
_Last udated: 02/2019_
## Aim of this guide
The aim of this guide is to list some tips and give advice on how to reduce your ecological impact in the beautiful city of _Maastricht_. It is mainly intended for foreign students with any dietary requirements, altough consumption of meat will be discouraged. 
This guide will also encourage other practices such as: avoiding Facebook for organisations (ethical reasons), buying locally (economical and social reasons)...
## Introduction
Maastricht is known to be a lovely city with a lot of international and motivated students. 
...
**WARNING**: some opinions in this document might be considered as edgy. It's okay, you don't have to agree with everything here. 
## Zero Waste!
Zero waste is strongly encouraged in this guide. Try to avoid buying plastic of any kind, plastic bags and packaging. Look at every little object that you throw and ask yourself: how can I avoid that? 

## Food
Although many students (including myself) often forget about this task, eating is one of the most important things to keep yourself active and your brain working.
The food you buy and eat can also have an enormous ecological, economical and social impact on the world... you can change the world by eating the right food!

### Buying food
#### Food to avoid

#### ~~Supermarkets~~
One of the first things that people notice when they try to go green is that it is extremely difficult, if not impossible, to have a good ecological impact when going to supermarkets. Every product is packed in a lot of future garbage and consumed fuel to be transported. Also remember that you are not only paying for the product but also for the plastic and the marketing. So if the prices are low, the quality of the product is probably really bad. 

If you really really really need to go to a supermarket, always remember to bring your own recyclable bags for fruits and vegetables and always choose glass containers. Glass is absolutely amazing, can be recycled almost to infinity and has a low production impact.

_also read paragraph on recycling_
#### Local market (Markt)
The local market is organised every wednsday and friday of the week at the Markt (dah!). You can find there many fresh inexpensive fruits and vegetables as well as other local products. Always remember to bring your own bags and containers to avoid using plastic!

It can also be a good occasion to practice your dutch (jk, it doesn't work).
#### Other food shops
There also are some bakeries,
#### Organic Food? Organic food!
[articles on why organic food is good]
### Coffee
Coffee consumption is not advised. But if you're already addicted to coffee and really can't live without it, you can simply bring your own cup to drink from the coffe machines. Most (and probably all) of them allow you to put your own cup to drink from. If you study at Tapijn Learning Spaces, a kitchen area is also provided and provides cups, a boiler and shared goods such as tea and coffee.
### FoodSharing
Located in the Tapijn area, this place offers
### Places to eat
#### TooGoodToGo

#### Cato-by-cato
This place provides many vegetarian, vegan as well as meat options for 3,75€ (as of 2019). It also allows you to bring your own empty food container from home so that you do not have to waste any of their cardboard containers.
#### Food bank
What is food bank? <Insert their own description here>
Every friday, they provide a free three course vegan meal (recommended also for non-vegans) made out of leftovers from the Markt. They usually go take vegetables at around 15:00, start cooking at 17:00 and serve food at around 20:30. Great opportunity to socialise and explore new vegan meals ideas. 
Anyone can join at any moment.
Plates, forks and knives are not made of plastic! The only waste produced is the one left by the market.
    
**Where**: LBB
**When**: every Friday.

#### Other places
More and more places provide take-away food without any waste! You can for instance buy a sandwitch at [The Lab](https://www.lab-maastricht.com/), the Cafeteria Toneel Academi, or the UM library without any plastic bag/containers/forks...
## Second hand
> If you don't always need something, share it. If you need some only once, ask someone to share it.

What can be bought second hand? Basically everything (except for food).
There exist some dutch online second-hand platforms (such as marktplaats.nl/), but buying locally is always better as it avoids transport and useless packaging.

Here is a list of second hand shops in Maastricht:
* [StudentBikeShop](http://www.studentbikemaastricht.nl/)
* [Library of things](https://sarahfitterer.wixsite.com/libraryofthings)
## Trasportation
Needless to say, having a car in Maastricht is mostly useless and unnecessary for city transports. Having a bike is of course a must and a really efficient and ecological way of moving around. You can buy good affordable second hand bikes at [StudentBikeShop](http://www.studentbikemaastricht.nl/).
### Traveling
Trains and buses are both ecological ways of moving (trains are relatively better, but more expensive). FlixBus and other bus companies allow you to pay some cents more to compensate your CO2 impact, which is also good.
If for some reasons moving by car is required, carsharing is always better. There exist many apps (such as BlaBlaCar) or you can also find some Facebook groups made for that.

Some cool ecological trips: 
* train to Bruges

## Recycling
### [Use less, recycle the rest!](https://www.greenofficemaastricht.nl/recycling)
Recycling is not a completely sustainable solution for the environment, especially for plastic, because:
* the production and transport of plastic has a big ecological impact
* recyclable waste is often transported in another city
* most of the recyclable waste is actually non-recycled

You can read more about this [insert link here].
It is however a good short term solution, and an important step for a more ecological society. 
### Battle of the elements
#### Glass beats paper!
Glass if by far the best 
#### Paper beats plastic!
#### Plastic beats... not much
## Ecology related communities/organisations 
### CNME
nature and community education volunteer organisation
### Tapijntuin
Small community garden
### Green Office Maastricht
Site: [greenofficemaastricht.nl](https://www.greenofficemaastricht.nl)
### Food Bank
See [paragraph food above](#Food-Bank).
## Get active!
Never criticise others for not having ecological habits, instead, share this guide to them and give them a good example! It won't be an easy immediate step, that is why it is important to start now by doing small steps.
> "Actions speak louder than words"
    - someone, probably

### If you are a developer
As mentioned in the introduction, I strongly do not advise the use of Facebook for many reasons (read something [insert link here]). That is why your help is needed to build networks, websites and platforms to help people connect and do amazing things. Some ideas to get you started:
- [ ] Environmental friendly to do lists 
- [ ] Reward-based app for ecological acts

### If you are a fancy business student
Provide new local solutions to global problems. Look into developing [parallel local currency](https://en.wikipedia.org/wiki/Local_currency), ...
### If you are an elegant law student
Provide support to squats and communities to have their voice being heard.
Help people having support from the government and change what needs to be changed.
### If you are ... something else?
Just do stuff, take cold showers, meditate... Trying to make Maastricht more green can give you infinite many ideas, infinite motivations and a sense of life (can't really assure for the last one). So get involved!
 
+++
title = "Todo"
date = 2021-05-15
description="List of things to improve on this website"

extra.sitemap_ignore = true
[taxonomies]
categories = ["Project"]
tags = []
+++

- [ ] clean up git history of this website
- [ ] Add quantities to vegan lasagna recipe, post on https://based.cooking/
- [ ] create qr page
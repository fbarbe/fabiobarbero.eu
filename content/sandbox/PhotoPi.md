+++
title = "🖼️ PhotoPi"
date = 2021-05-03
description="Building a smart photo storage"

extra.sitemap_ignore = true
[taxonomies]
categories = ["Project"]
tags = ["Raspberry Pi", "Python", "Image Recognition"]
+++

## Idea
An open-source version of [Monument](https://getmonument.com/) with better control and privacy.
"Automatic backup and organization for your photos", with location tag and image recognition, running on a Raspberry Pi.

## Features
### Backend
1. Face recognition (detection, matching, prediction)
2. Object detection
3. Scenery detection
4. Duplicate/similar picture finder
5. Encrypted data for each user
6. Share pictures among users
### Frontend
- 

## Software architecture
- [DeepStack](https://deepstack.cc/) running in the background for 1,2,3 (easy)
# fabiobarbero.eu
This repository contains the website of Fabio Barbero. It is generated with
[Zola](https://getzola.org/) and lives on Gitlab Pages.

## Development
This is a Zola site, so the ultimate source of wisdom ought to be the [Zola
docs](https://www.getzola.org/documentation/content/overview/). However, below you
can see a brief rundown of how to operate it and how it works.

### Running it locally
To set it up locally [install Zola](https://www.getzola.org/documentation/getting-started/installation/), clone the repo, `cd` into
it and execute
```
zola serve
```
Voilà, you now have a little development copy of the site running locally, presumably
on `http://127.0.0.1:1111`. Every time you make a change to the website files, zola
will rebuild it and you will automatically see the newer version.

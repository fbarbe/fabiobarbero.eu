+++
title = "🗒️ Bucket List"
date = 2021-04-28
description="My personal bucket list"

extra.sitemap_ignore = true
[taxonomies]
categories = ["Random"]
tags = ["List"]
+++

# Goals
- [ ] go zero waste
- [ ] learn German
- [ ] learn Russian
- [ ] commute exclusively by electric bike
- [ ] launch a startup

# Travel
- [ ] Go from Europe to Asia [by train](https://www.seat61.com/trans-siberian-railway.htm)
- [ ] Experience something on workaway/wwoof

# Health
## Sports
- [ ] learn to do Parkour
- [x] run a half marathon
- [ ] run a marathon
- [ ] do a half iron man
- [ ] do an iron man
- [ ] hike somewhere for a week without technology
- [ ] sky dive
- [ ] climbing
- [ ] paragliding

# Music
- [ ] publish an album of different genres
- [ ] learn jazz on guitar
- [ ] learn to drum
- [ ] play live in a stage

# Skills
- [x] learn to juggle

# Other
- [ ] have a discussion with a homeless person
- [ ] have a game night with Cameron Browne
+++
title = "📅 What a year!"
date = 2021-12-22
description="Brief summary of what I achieved in 2021"

[taxonomies]
categories = ["Random"]
tags = ["Challenge"]
+++
# What happened this year?
2021 has been quite a year for me. I graduated from university, published two papers, got a job as a Software Developer and, most importantly, focused on my mental and physical health. I gave myself [quite a few challenges](/posts/challenges), from only taking cold showers for a month to doing more sports. This post will summarize a few things I did this year, mainly for personal recalling.

Ok so there's also this thing called global pandemic or something. I have been extremely lucky to not be affected too much by it. I was still able to meet the people I like and get to know even more awesome people. My achievements wouldn't have been possible without them, so a special thanks to everyone who has spent time with me this year. :D
## Health
I gave myself the goal to do 365h of sport activities this year. It is currently the 363rd day of the year, and I currently recorded 360h of sports! Some long Yoga sessions and runs should get me to my goal, but even if I do not manage to reach that exact number I am very happy with the progress I've done.

My main focuses have been running and yoga. I also did a few ~40-80km bike tours and whole body workouts, but not with the same frequency.

Meditation has been a very useful tool to approach life with a more mindful (or better, _kindful_) attitude. A challenge I gave myself often was to bathe in cold water such as on lakes/rivers at the top of Austrian and Italian mountains.
### Running
I started running in December 2020 and am really happy of my results this year. On average, I tried to run 2-3 times a week, but reduced my load when I started working in September to once a week.
Here are a few achievements:
- Ran 21 km during the Bataverien race: The Bataverien race is a relay race organized in the Netherlands. Participant participate in teams and can choose the distance they will run, as long as the team reaches a certain distance. I had no intention to run my first half marathon, but felt great while running and decided to hit this milestone.
- Finished the 20km of Brussels in 1h44. This was a lot of fun! I'll definitely try to participate again in the next few years.
### Yoga
Yoga was also a discovery from end of 2020. This year, however, I took my Yoga practice to the next level: I did more than 146h of Yoga, and had a streak of 162 consecutive days of Yoga which was interrupted by mistake.
## Travelling
My main achievement regarding travelling this year is that I did not take the plane once to go anywhere! I commuted exclusively by bus, train and shared car rides.
This year, I have been in the following countries:
- Belgium
- Netherlands
- Germany
- Italy
- Slovenia
- Austria
- Denmark

Was it fun? Yeah! Well ok, sleeping on a bus is not always super fun, but there is a great sense of achievement for not polluting the environment by taking the plane, and experiencing the whole landscape from point A to point B.

## Learning
There are quite a few things I learned this year! These go from juggling with 3 balls to creating this website. Writing a short list of what I did every day also helped me realize what I improved on every day.
### Academic
After three year of hard work (writing this for my future employer), I obtained a bachelor degree in Data Science and Artificial Intelligence at Maastricht University with Cum Laude! I also managed to [publish two papers](/portfolio), and presenting one of them (online) to the IEEE Conference of Games.

### Music
I continued to fuel my passion for Jazz by learning more improvising techniques (including Neo Soul, which I absolutely love), and am really happy to have learned the jazz standard Giant Steps and played `the lick` on a multitude of different instruments (piano, keyboards, guitar, organ, harp, Tesla tablet, glass cups, ...)

I also listened to more than 180 full albums while working of a multitude of genres.
### Reading
In 2020, my bookshelf consisted almost exclusively of books I read in high school. It still does (in fact it contains even less books than before since I gave them away), but I did read a lot of interesting books this year! I kept an updated list of my books in my [challenges post](/posts/challenges).
### fabiobarbero.eu
If there's one thing I'm really happy about this year is this website :). It did not (and still doesn't really) have any specific goal, but helped me learn quite a few thing. I was also really happy to see that [my post on how to make a bot on Signal](/posts/signalbot) got well over 2000 clicks from Google alone!

Writing posts like this one also helped me organize my thoughts and get ideas on what I could improve next.
### Work
I got my first job this year as a Software Developer at [Odoo](https://www.odoo.com/r/d8g), after spending quite some time in the tedious task of applying for job positions. There are quite a few things I enjoy about this job: the environment is stress-free, the inner management of the company and projects is amazing and I am happy to contribute to Open Source code (mostly). Having to spend 5 days a week in front of a screen is a bit tiring, and I'm curious to see how I will handle that in the future as I'd love to have a more active lifestyle.

### Other projects
After getting experience with Open Source project at my company, I also did some minor contributions to two open source projects: [aw-spotify-watcher](https://github.com/ActivityWatch/aw-watcher-spotify) and [Ludii](https://github.com/Ludeme/Ludii).

I also have to thank my awesome friend Moritz for getting me into the [HomeHaven](https://homehaven.nl) project, and doing most (if not everything) of the work. I am truly excited about the future of this project and would love to dedicate more time to it in the near future.


# So what's next?
As a general idea, I'd love to continue doing more challenges! They truly motivated me at the start of the year and made me discover a lot of cool things.
Here is a short list of things I'd like to do in 2022:
- Continue having a regular running and Yoga routine.
- Meditate more regularly.
- Commit to a stricter/healthier Vegan diet.
- Take active decision for doing a master or continue working
- Improve my German
- Read one book a month
- Possibly publish another paper
- Practice more technical exercises at the piano
- Practice more musical instruments (saxophone, guitar)

I'll also try to continue updating this website (and perhaps cleaning up the messy Gitlab repository...).
I wish you, kind reader, a really happy 2022!
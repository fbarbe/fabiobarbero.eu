+++
title = "🐾 Backtracking from (healthy) overwhelmedness"
date = 2025-02-15
description="🚉 On Trains: Post #7"

[taxonomies]
categories = ["Random"]
tags = ["On Trains"]
+++

It's quite simple, really. For [my thesis](/posts/ontrains-6/), I'd still like to:
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/markmap-toolbar@0.18.10/dist/style.css"><link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.16.18/dist/katex.min.css">

<style>
* {
  margin: 0;
  padding: 0;
}
#mindmap {
  display: block;
  width: 100%;
  height: 100vh;
}
</style>
<svg id="mindmap"></svg>
<script src="https://cdn.jsdelivr.net/npm/d3@7.9.0/dist/d3.min.js"></script><script src="https://cdn.jsdelivr.net/npm/markmap-view@0.18.10/dist/browser/index.js"></script><script src="https://cdn.jsdelivr.net/npm/markmap-toolbar@0.18.10/dist/index.js"></script><script>(()=>{setTimeout(()=>{const{markmap:x,mm:K}=window,P=new x.Toolbar;P.attach(K);const F=P.render();F.setAttribute("style","position:absolute;bottom:20px;right:20px"),document.body.append(F)})})()</script><script>((b,L,T,D)=>{const H=b();window.mm=H.Markmap.create("svg#mindmap",(L||H.deriveOptions)(D),T)})(()=>window.markmap,null,{"content":"todo","children":[{"content":"Visualisations","children":[{"content":"Isochrones","children":[{"content":"maximum distance given train infrastructure","children":[],"payload":{"tag":"li","lines":"7,8"}},{"content":"best connections given timetable data","children":[],"payload":{"tag":"li","lines":"8,9"}},{"content":"distance by car","children":[],"payload":{"tag":"li","lines":"9,11"}}],"payload":{"tag":"h3","lines":"6,7"}},{"content":"Distorted maps","children":[{"content":"distorted map for large cities","children":[],"payload":{"tag":"li","lines":"12,13"}},{"content":"publish a video responding to <a href=\"https://www.youtube.com/watch?app=desktop&amp;v=rC2VQ-oyDG0\">this video</a>","children":[],"payload":{"tag":"li","lines":"13,14"}}],"payload":{"tag":"h3","lines":"11,12"}}],"payload":{"tag":"h2","lines":"5,6"}},{"content":"State of open data","children":[{"content":"Explore RINF data","children":[],"payload":{"tag":"h3","lines":"15,16"}},{"content":"Evaluate and track the state of public GTFS files","children":[],"payload":{"tag":"h3","lines":"16,17"}},{"content":"Compare and contribute to <a href=\"https://transitous.org/\">Transitous</a>","children":[],"payload":{"tag":"h3","lines":"17,18"}},{"content":"Compare performance of open trip planners on consumer hardware","children":[],"payload":{"tag":"h3","lines":"18,19"}},{"content":"Contribute to wikidata for incomplete city data","children":[{"content":"write a bot to do so automatically","children":[],"payload":{"tag":"li","lines":"20,22"}}],"payload":{"tag":"h3","lines":"19,20"}},{"content":"Openstreetmap","children":[{"content":"fix incorrect UIC station codes","children":[],"payload":{"tag":"li","lines":"23,24"}},{"content":"write a tool to find inconsistencies in rail tracks for routing","children":[],"payload":{"tag":"li","lines":"24,26"}}],"payload":{"tag":"h3","lines":"22,23"}}],"payload":{"tag":"h2","lines":"14,15"}},{"content":"Connectivity metrics","children":[{"content":"compute weighted connectivity given pairs of cities","children":[],"payload":{"tag":"li","lines":"27,28"}}],"payload":{"tag":"h2","lines":"26,27"}},{"content":"Other","children":[{"content":"Start writing my thesis","children":[],"payload":{"tag":"h3","lines":"29,30"}},{"content":"Publish a nice git repo with all my code","children":[],"payload":{"tag":"h3","lines":"30,31"}},{"content":"Contribute to projects","children":[{"content":"OSRM","children":[],"payload":{"tag":"li","lines":"32,33"}},{"content":"Motis","children":[],"payload":{"tag":"li","lines":"33,34"}},{"content":"...","children":[],"payload":{"tag":"li","lines":"34,35"}}],"payload":{"tag":"h3","lines":"31,32"}}],"payload":{"tag":"h2","lines":"28,29"}}]},{"colorFreezeLevel":2})</script>

This is just a small subset of things that came to mind while writing this. The actual list of ideas to do would result in a much bigger diagram.

From my experience, in research projects, it's quite common to have cyclic branching and broadening phases. Diving deep into a topic, but zooming back to the bigger picture when too many new elements are discovered and need re-evaluating. Writing a daily log of what I do (using [Obsidian](https://obsidian.md/)) has greatly helped me to keep track of the bigger picture, and avoid feeling overwhelmed by random ideas floating around.

However, I still find it hard to cut down branches of research. Of the diagram above, I have already started working on almost all tasks. However, none of them is finalized.

It is perhaps time to set a deadline, after which I would stop exploring and start writing my thesis.

*This shorter entry was necessary for organizing my thoughts. In two weeks: isochrones! (hopefully)*

*Note: no text in this document has been generated or rewritten by a Large Language Model.*
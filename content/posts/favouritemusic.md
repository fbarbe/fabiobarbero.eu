+++
title = "🎶 Music I Recommend"
date = 2021-05-13
description="Collection of my favourite songs and artists"

[taxonomies]
categories = ["Random"]
tags = ["List", "Music"]
+++

There's a chance you're here after a discussion we've had about music. I've decided to write a collection of artists/songs I particularly like. I will try to keep this updated, and feel free to share any of your music with me!

> Note: I listen to a lot of music weekly, and am struggling a bit to keep this list updated as I keep discovering new cool artists. Consider this as "Some Music I Recommended in 2021" ;)

## Most listened genres
Like a lot of people, I like to listen to a large variety of styles from classical music to tecno. There are however two styles I listen to the most: _jazz_ (especially _contemporary jazz_) and electronic music (such as _future bass_).

## Most listened artists
Here are the artists I listen to and most recommend. The list is not ordered precisely, but I do keep the artists I like the most on top:
### Anomalie
I usually do not like artists who keep making songs in the same style but I definitely have to make an exception for Anomalie. His style of music (_Jazztronica_) is so unique and fascinating.

Top picks:
- _Metropole_ album, [I](https://open.spotify.com/album/2DnmCXa8xsCuCYmA3rUbbU) and [II](https://open.spotify.com/album/5tjtkMg20jEuzuJrT27gBj)
- [Odyssee](https://open.spotify.com/track/5eYuwdQzr0D4NuPV3mYQLT)

### Tigran Hamasyan
Armenian pianist with his own very unique variety of styles. His may sound unusual to an occidental ear.

Top picks:
- [Drip (Berklee Middle Eastern Fusion Ensemble Studio Recording)](https://www.youtube.com/watch?v=z7j7bdEPSd0), which is by far one of my favourite songs
- [The Poet](https://open.spotify.com/track/7oJgLeTIETiDEqRFaWbS3k), from the album [Shadow Theater](https://open.spotify.com/album/3cH2LQH41G3PO5XxmLVxJo)

### Jesus Molina
Third pianist in a row in this list, Jesus Molina is undoubtably the most skilled in the list. His technique is over the top and he his highly influenced by Oscar Peterson.

Top picks:
- [Departing](https://open.spotify.com/album/17LSsxYGDZZaJytHHzc7nL), in the album by the same name
- [Keyscape Sessions - JESÚS MOLINA & EMILY BEAR](https://www.youtube.com/watch?v=NpSFcL9fASA)
- [Justin-Lee Schultz and Jesus Molina Jamming at the Namm Show 2020](https://www.youtube.com/watch?v=fTGKpd2XJzw) these last two sessions are truly mindblowing and will make any pianist shortly depressed.

### Avishai Cohen
Avishai Cohen is an amazing jazz bass player. His trio plays with a lot of different time signatures and polyrithms.

Top picks:
- [Pinzin Kinzin](https://open.spotify.com/track/4xWlLo2D84a8MX2GQObKSF), from one of my favourite albums, [Gently Disturbed](https://open.spotify.com/album/1YpYJkg18DHN92lVz9qcYC) (try to guess the time signature of the song)
- [Two Roses](https://open.spotify.com/album/2szbf6gQqHlk7cogeEMBfg)


### Domi & JD beck
These two are... weird... As of when I'm writing this they haven't released an album yet but have shown their incredible skills on Youtube videos and events.

Top picks:
- [Flintstones theme](https://www.youtube.com/watch?v=QQ0cLpNRXCQ)
- [BC invaders](https://www.youtube.com/watch?v=skEUnT98cWk)
- [Jump](https://youtu.be/4xPYv_mqviY)


### Jacob Collier
Jacob Collier is now fairly known. He is a very talented instrumentalist, exploring a whole bunch of different genres with some crazy music theory behind. Truly a musical genious. I don't quite like his new stuff (more pop) as much as his old.

Top picks:
- [Fascinating Rhythm](https://www.youtube.com/watch?v=K28H04Y2IdE)
- [Flintstones](https://www.youtube.com/watch?v=zua831utwMM)
- [Close to you](https://www.youtube.com/watch?v=9s1baxrxGHU)
- [Djesse Vol 1](https://open.spotify.com/album/47bMDS4CMLbqcIVjEMWUjK)
- [In my room](https://open.spotify.com/album/4yVaTn71IU28NA0Et4bRED)
- [Nebaluyo](https://open.spotify.com/track/2kP72xV4t4LTcuGUqheQkH)

### Hiromi
Yet another jazz pianist... Hiromi is a Japanese pianist who truly puts her emotions in the music she plays.

Top picks:
- [Blackbird](https://open.spotify.com/track/1NpTkYFGPrYb9FRNGjLVyJ)
- [Pathetique](https://open.spotify.com/track/08kouBwoppDP70MmwiNUPr)

### Snarky Puppy
The most known contemporary jazz group. I don't really listen to them anymore as their style is pretty repetitive but contains a lot of very unique aspects that influenced a lot of other modern jazz artists.

Top picks:
- [The curtain](https://open.spotify.com/track/29ls98FgNdZHbmqdQeF7E6)


### Other artists
_I didn't have time to write a full description for all of these artists, so here's a list_:
- Eugen Cicero (_jazz, swing_)
    - [Sunny](https://open.spotify.com/track/03kaOmvT2zgtWf8B0lChLs)
    - [Ah! Vous Dirais-Je, Maman](https://open.spotify.com/track/2hJ9C3uLtHgJG8g1YeGGYD)
- Oscar Peterson (_jazz, swing_)
- Glenn Gould (_classical_)
- George Gershwin (_jazz, swing_)
    - [Rhapsody in Blue](https://open.spotify.com/track/7rgCuCBu2U4LipyvjV3nV0), one of my favourite songs
- Chick Corea (_jazz_)
- Herbie Hancock (_jazz_)
- Jacques Loussier Trio (_jazz_)
    - [Play Bach](https://open.spotify.com/album/6qxDNVwoFgGlvlttjnGlnt)
- Rob Araujo - similar to Anomalie (_jazztronica_)
    - [Nineteen](https://open.spotify.com/track/6xEHCWUvalb0fNYuAo591v)
- Savant (_electronic, EDM_)
    - [Melody Circus](https://open.spotify.com/track/3hn2s09PyNrYSqCnhYjm7L)
- Joey Alexander (_jazz_)
- The Funky Knuckles - very similar to Snarky Puppy (_funk, contemporary jazz_)
    - [16 bars](https://open.spotify.com/track/1AFJdPatKBaoHzT0UFRBsM)
- Lydian Collective (_jazz_)
    - [Adventure](https://open.spotify.com/album/6qMB487YzABEnYaqoCtw91)
- 20syl (_hip hop, electronic_)
- Jacob Mann Big Band (_funk, jazz_)
    - [Baby Carrots](https://open.spotify.com/track/1rqQjvJPZbGTpVxdYwvDdn)
- Art Tatum (_jazz_)
    - [Tea for Two](https://open.spotify.com/track/0Otf1ZfYNIjhqFIuJk0fsy)
- Kamasi Washington (_contemporary jazz_)
    - [The Epic](https://open.spotify.com/album/2j2q2ySuVk43eHB8wI5XQj)
- Dirty Loops (_pop, funk_)
- Louis Cole (_pop, funk_)
- Polyphia (_progressive rock_)
    - [G.O.A.T.](https://open.spotify.com/track/0maCwhZTO3PybhSiQcsjAf)
- Caparezza (_italian rap_)
    - [China Town](https://open.spotify.com/track/0sWPru0EpQZtymTLiRYm9o)
- Theolonius Monk (_jazz_)
- Bill Wurtz (_jazz-pop_)
- Vulfpeck (pop, funk)
- Chuck Sutton (_electronic_)
- Diveo (_electro-pop_)
    - [We're beautiful](https://open.spotify.com/track/3V2PU0Bd7r8PjauFRDZlMv)
- Supertramp (_rock_)
- Igorrr (_experimental, extreme metal_)
    - [Savage Sinusoid](https://open.spotify.com/album/2NsGk9oBBBMfblYdLcjYhu), one of my favourite albums
- C2C (_pop, EDM, electronic_)
- Cory Henry (_jazz, gospel_)
    - [NaaNaaNaa](https://open.spotify.com/track/3Q94flBRFGAqaCrebWRbno)
    - [Donna Lee](https://open.spotify.com/track/6CYFEVhc3BdAaMcS0G4P68)
- Ryuchi Sakamoto (_ambient, electronic, classical_)
    - [Forbidden Colours](https://open.spotify.com/track/5jMGwgpkZlQ747VO7Qp5GX)
- Weather Report (_funk_)
- Queen
- Shaun Martin (_jazz, funk_)
    - [The Yellow Jacker](https://open.spotify.com/track/6ve32j67lHghOs4TOV8OpL)
- Joachim Horsley (_rumba_)
    - [Beethoven in Havana](https://open.spotify.com/track/7oTYeO1imnOARszuSV8wU9)
- Bigflo & Oli (_french rap_)
- Justice (_tecno_)
- Antiloops (_jazz_)

### Other songs/albums I like
- [L'oiseau qui dance](https://open.spotify.com/track/3KKBT4UfWIP3fBMXn5Oprj)
- [Strasbourg / St. Denis](https://open.spotify.com/track/62VWmsNoDmqT0Mj9oHHFVh)
- [Tadow](https://open.spotify.com/track/51rPRW8NjxZoWPPjnRGzHw)
- [It runs through me](https://open.spotify.com/track/02CygBCQOIyEuhNZqHHcNx)
- [polyriddim](https://open.spotify.com/track/4dAQGQETg8n4BKl1ahUHYq)
- [Fashion](https://open.spotify.com/track/2QdIKrSFxPrHmc2bf2AUth)
- [Mr. Blue Sky](https://open.spotify.com/track/2RlgNHKcydI9sayD2Df2xp)
- [Chpinklez](https://open.spotify.com/track/32ndTJ5pnYtf41jW3dnlDf)
- [Prelude in C-Sharp Minor, Op.3, No. 2](https://open.spotify.com/track/3rd1Chqzxr95fcyBaJl0JZ)
- [Flight of the cosmic hippo](https://open.spotify.com/track/0dDZPziZGcQV4bUBaKfzyJ)
- [Fergal's Tune](https://open.spotify.com/track/0lZYNaLYFDZHQfQUxiXMK2)
- [Orinoco Flow](https://open.spotify.com/track/0Fyj9w0HVfjoxm9S8nGClt)
- [Bruises](https://open.spotify.com/track/59OZiGqFfWmry6o9pTx8i8)
- [Letter for E](https://open.spotify.com/track/6271EWRKlUv7TPikOx1VUw)
- [Pelican](https://open.spotify.com/track/07vQPvFNwHGNmCvR3vBTUs)
- [Funck](https://open.spotify.com/track/3R4b72wiBM1sB6V0i8IHaA)
- [Atbėgo Kariūnai, Sušaudė Brazauską (SSG cover)](https://www.youtube.com/watch?v=qZVSzOzHlGE)
- [Side Bar](https://open.spotify.com/track/7z2tT2eXxAnO256l8ufnst)
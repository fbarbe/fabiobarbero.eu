+++
title = "🚉 On Trains: Introduction (Post #0)"
date = 2024-11-09
description="My first post of many, on data and on trains!"

[taxonomies]
categories = ["Random"]
tags = ["On Trains"]
+++

## Short context
I spent quite a few months looking for a thesis topic that I'd be interested in working on, to finish my Master. For the past 3 years, I've enjoyed less and less spending time on my laptop, working on issues that seemed irrelevant or unproductive both in terms of societal and personal impact. Moving my body, interacting with people, working towards a common goal are things I value more and more. And the Master I had started became more and more of a burden. However, a big part of me is unable to leave things unfinished. This can be both a blessing and a curse, but it has often been a curse when forcing myself to spend time on things I no longer enjoyed.

But I'm getting lost in my stream of thought. When looking for a topic, it was clear to me that it'd have to be something I could work on from anywhere in the world. On trains, mostly. After spending 7 months in Montreal, where commuting outside the city is only feasible by car (on a budget), I was very much missing European train lines, Interrail tickets, and my friends scattered around Europe. Working on trains was one of the few things I was certain I'd do.
And then it struck me. I'd do my thesis on trains on trains. I found it so funny that I decided to do it. I particularly like trains, for their sustainability, and the freedom they give me. They also give me a feeling of adventure. So after looking for a professor at my university that would be interested in being my supervisor, I drafted a Master thesis proposal for analysing and optimising the rail network in Europe (Working title: "Finding the Missing Trains: An Analysis and Optimization of Europe's Passenger Rail Network").

This isn't exactly the first time I work on a project completely on my own, but it is the first time the entirety of the project comes from me. I've set myself a bunch of tools and guidelines to stay on track, mostly to make sure I don't overwork myself and keep a healthy relation to this project. One of the goals I have is to write a series of blogpost, once every two weeks or so, to keep track of my progress and present my findings in a clear manner. It's an opportunity to step out of my rabbit holes and share any interesting findings. They will of course be writing on trains too! Today's blogpost was written on the IC 9235 to Amsterdam.

What can you expect from these posts? I don't know. I'm hoping to write some useful guides about how to deal with transit data, a lot of bad puns with the word "train" (can't wait to train a train model), a few rants and investigations on weird things I encounter...

Please reach out if you have any ideas! I would genuinely love to interact with many people throughout the next 5-6 months of my thesis, so don't be afraid to send me an email.

But expect the unexpected. In fact, here's a segment in German I wrote on the 11th of October (thank you, [DeepL Write](https://www.deepl.com/de/write)):

## ICE 315 nach Frankfurt
Hier bin ich. ICE 315 nach Frankfurt. Platz 86 im Wagen 24. Der Zug ist gerade ziemlich voll. Mein Laptop passt nicht auf den Tisch im Zug, weil da noch ein Laptop steht. Zwei sogar. Er liegt fast bequem auf meinen Oberschenkeln. Viele Menschen zwitschern am Telefon oder miteinander. Draußen ist es sonnig und die Landschaft ist besonders schön. Ich bin auch besonders glücklich. Züge machen mich oft so. Natürlich ist es nicht immer einfach, mit dem Zug zu fahren. Die Fahrt heute war auch nicht so einfach. Einige Züge sind ausgefallen und ich bin eine Stunde zu spät. Das Schlimmste war, dass ich in Liege-Guillemins bleiben musste.

## Working on a project alone
Productivity. Such a scary word. Some people will make their bodies tired, sick, cut ties to other people in the name of that word. I have certainly done similar things in the past.

But productivity is fascinating. The skill to be able to do something in the fastest, most efficient, and easy way. I want to emphasise the importance of productivity in making things easy. Sometimes, it can even make them fun! Being able to retrieve a piece of information with a simple search, instead of manually looking through hundreds of documents. Yay, less time on a screen!

Of course, setting up and learning to use productivity tools also takes time. Saving downloaded files with a sensible name and in a proper location takes longer than accepting the default `d293LCB5b3UncmUgYSBuZXJk.pdf`. This is of course not only true in the digital word, but also applies to organising your bedroom. It is hence important to find a good balance between spending time organising to do things faster and not micromanaging everything.

My priority throughout the next months will be to stay healthy, and have a positive impact on the people around me and the world. For that, I find it important that the time I spend on my thesis is well thought, and fun!

Here are a number of tools/actions I have currently decided to implement:
1. **Writing down everything you do**. By far the best advice I got from friends. For large projects like this one, writing down everything I do has been a great way to feel less stressed about retrieving information I found in the past, and having a better sense of progress and of what still needs to be done. This is the first time I try doing so. I am using [Obsidian](https://obsidian.md) to keep track of my notes in Markdown, using their simple daily notes feature.
2. **Saving all papers on [Zotero](https://www.zotero.org/)**. This one is a no-brainer at this point. I have used it in many projects, and having a place that saves all the papers I find, and can automatically create a Bib file for LaTeX at the end saves a lot of time.
3. **Using git, even though I'm alone**. I haven't really benefited from this one yet. This came from [Sebastian Lague](https://www.youtube.com/@SebastianLague), who is an amazing storyteller. He explained in an FAQ that he uses git to keep track of process, and allows him to play around ideas a lot more. So far it hasn't caused me too much hassle, so I'm happy to continue exploring how it can make my life easier.

There are many other small tricks I use for coding faster. Copilot and ChatGPT are of course of great help for sketching out code fast. I try to play around with new libraries and code ideas in general in a Jupyter Notebook, to then move it in a more organised python file with command line commands (using `argparse`) in a more organised folder structure.

## So what have I done so far?
It's been almost a month since I started working on this project. In the past month, I explored different data sources, got familiar with terminology, libraries and standards, reached out to relevant organisations and started playing around with some data.

In practice, I have explored the GTFS (**G**eneral **T**ransit **F**eed **S**pecification), and **O**pen **S**treet **M**aps (OSM) data.
There are many things I want to rant about and exciting results I want to share (such as replicating the chronotrains.com website in Python), but I believe that will have to wait for another blogpost.

My next steps will be reading a lot of the literature, which is something I am always afraid to do (I hate PDFs), but should probably be done soon. Reaching out to organisations has given me some fruitful insights, but I yet have to find someone with whom I can have a proper chat, or who is interested in potential collaboration.

I am still awaiting feedback from my supervisor on my Thesis proposal. Once that will be done, I will have 5 months to start working on it.
## Closing thoughts
I quite enjoyed writing this blogpost! It was quite difficult to start writing it, but I am quite happy with the result. 
There are so many more things I was planning to write about!

From reading blogpost around the internet (such as the amazing [Science Stories](https://violahruzzier.substack.com/) by Viola Ruzzier, who I was lucky to meet in Montreal), I realised how much figures and drawings can enhance the reader's experience and make it more fun. I unfortunately can't draw, but I might come up with something in the future!

*Note: no text in this document has been generated or rewritten by a Large Language Model.*
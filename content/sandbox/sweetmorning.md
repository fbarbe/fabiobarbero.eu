+++
title = "🌅 Morning Conditioner"
date = 2021-07-09
description="Starting the day with words from the previous day"

extra.sitemap_ignore = true
[taxonomies]
categories = ["Project"]
tags = ["Raspberry Pi", "Python", "Image Generation"]
+++

## Idea
Words/images have a deep impact on our subsequent set of actions. Waking up reading words decided the night before can help fixate goals and a gentle reminder of what you have to do.
### Design
- When going to bed (1), users enters words (2)
- During the night, a computer generates a picture based on these keywoards. (3)
- When the person wakes up (4), the words and image are displayed (5)

### Ideal implementation
1. Computer detects when person goes to sleep
    - Connected to smartwatch
    - Manual entry
    - Light switch
2. Voice recognition can be used to enter the words
3. A Coral TPU can be used with OpenAI's DALL-E and CLIP
4. Simple alarm/connected to smartwatch
5. Computer turns on projector or screen

Virtual assistant tasks:
- clone someone's voice "can you clone their voice?"
- set timer/alarm/reminder
- tell the weather
- play music on spotify

+++
title = "🍲 Favourite vegan recipes"
date = 2021-05-03
description="Building a smart photo storage"

extra.sitemap_ignore = true
[taxonomies]
categories = ["Random"]
tags = ["Vegan", "List"]
+++

This is just a list for myself with some of my favourite vegan recipes. I always try find recipes that are vegan by nature and don't need any ingredient replacement.

- Focaccia
- Pizza marinara
- Curry pot
- Cauliflower rice
- Risotto
- Vegan lasagna
- Spaghetti aglio e olio
+++
title = "Should I stop"
date = 2021-07-11
description="Thoughts on knowning when to stop."

extra.sitemap_ignore = true
[taxonomies]
categories = ["Thought"]
tags = ["Zibaldone"]
+++

I'm in a small town in Denmark, cycling in a city bike rented at the cheapest place in Copenhagen in Winter 2020. The bike gears don't really work and the the bike is not intended for anything else than a short ride in the city. But me and my two other friends did not think of that when leaving. We are now 50 km away from Copenhagen, with around 20km in front of us. I had never done anything like that before. I knew I could do it and that I wouldn't quit. Not having any worries or doubts, something that multiple people told me about later, helped getting there. I'll still remember it as one of the best out-of-comfort-zone experiences I've had.

_to be continued_
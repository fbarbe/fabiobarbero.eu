+++
title = "My Portfolio"
+++

# CV
You can find my Curriculum Vitae [here in pdf](/CV.pdf) format or [here in json](/cv.json) format.
You can also have a look at [my GitHub](https://github.com/fbarbe00) and [Google Scholar profile](https://scholar.google.com/citations?user=i1v0LD4AAAAJ).

# Portfolio
## Multi-Modal Embeddings for Isolating Cross-Platform Coordinated Information Campaigns on Social Media
*Tags: NLP, Graph Embeddings, Graph Neural Networks*

For an academic project in my first year of the master, we explored different techniques for identiying coordinated campaigns on social media campaigns. Our results were published at the [5th Symposium on Multidisciplinary International Symposium on Disinformation in Open Online Media](https://event.cwi.nl/misdoom-2023/).

- <i class="fa fa-book"></i> [Published paper](https://arxiv.org/pdf/2309.12764.pdf)

## Heuristic Sampling for Fast Plausible Playouts
*Tags: AI, Board Games, Game Length, [Ludii](https://ludii.games/), Java*

The work done for my bachelor thesis result in a publication with my supervisor, [Cameron Browne](http://cambolbro.com/)
The paper proposes Heuristic Sampling (HS) for generating self-play trials for games with a defined state evaluation function, with speeds comparable to random playouts but game length estimates comparable to those produced by intelligent AI agents. 
HS produces plausible results up to thousands of times faster than more rigorous methods.

- <i class="fa fa-book"></i> This paper was published at the [IEEE CoG 2021](https://ieee-cog.org/2021/assets/papers/paper_313.pdf).

<hr>

## PySeidon - a maritime port simulation framework
*Tags: python, geoplotlib, geojson, esper, fysom*

Software project born as part of [MaRBLe 2.0](https://www.maastrichtuniversity.nl/research/dke/honours-programme).

_Pyseidon_ is a simulation framework that allows developers to build, execute, and gather data of a maritime port simulation. A model of the Port of Rotterdam was built and scenario testing was performed on that model. We make PySeidon freely available as open source in the hope that the research community benefits from this framework. 

- <i class="fab fa-github"></i> [Repository](https://github.com/pyseidon-sim/pyseidon)
- <i class="fa fa-book"></i> Pyseidon resulted in a publication at the [ICCMS 2021](https://dl.acm.org/doi/10.1145/3474963.3474986).

<hr>

## University semester projects
### Simulation of the Solar System / Landing on Titan
*Tags: python, geoplotlib, geojson, esper, fysom*

The entire Solar System was simulated in Java from scratch, with interactive visualisation. A full (imaginary) mission to Titan was planned, including: computing trajectory, calculating fuel consumption.

- <i class="fa fa-file-powerpoint-o"></i> [Presentation](https://md.kszk.eu/p/r1TIX8W64#/)
<hr>

### Finding chromatic number
*Tags: Java, graph colouring*

The goal of this project was to compute the [chromatic number](https://en.wikipedia.org/wiki/Graph_coloring#Chromatic_number) of a graph as fast as possible in Java.

<hr>

### Data Analysis course project
*Tags: python, seaborn, APIs (spotify, musicbrainz, audiodb, apple music)*


The goal was to find the most representitive Department of Data Science and Knowledge Engineering song, based on a YouTube music playlist that was shared among students attending the Data Analysis course that everyone could freely edit. 

Multiple music APIs were used to craft a dataset that had interesting features of the songs appearing in the playlist such as tempo, key, danceability, etc. Findings can be seen in the aforementioned video.

- <i class="fa fa-video-camera"></i> [Youtube video](https://www.youtube.com/watch?v=AU491GOg7No)
<hr>

## LaciCloud
*Tags: PHP, cloud storage, testing*

[Lacicloud](https://web.archive.org/web/20170731070717/https://lacicloud.net/) was a "first-of-a-kind ftp-based privacy-centric cloud storage solution", mostly developed by my high school friend Laszlo Molnarfi.

A very stretched, old picture of me can be found on the [about us page](https://web.archive.org/web/20170731070744/https://lacicloud.net/about_us/). I was 15 years old when I "joined the team", learning some PHP/server administration and helping mostly with testing and feature suggestion. This project further spiked my interest in programming and self hosting.

Me and Laszlo presented this project at the 2017 European Schools Science Symposium and won the 2nd price.

References:
- <i class="fa fa-globe"></i> [Lacicloud (archived)](https://web.archive.org/web/20170731070717/https://lacicloud.net/)
- [Interview with Lazslo Molnarfi (Lacicloud), Payiota founder](https://iotahispano.com/interwiew-with-lazslo-molnarfi-lacicloud-payiota-founder/)
- [ESSS: European Schools Science Symposium - EEB1](https://web.archive.org/web/20210417121830/https://www.eeb1.com/projets-2/esss-european-schools-science-symposium/)
- [Ready to participate in the Science Festival?](http://eurscmag.eu/?p=1070)
<hr>
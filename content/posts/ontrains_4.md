+++
title = "😤 Frustrations when exploring Open Source project"
date = 2025-01-04
description="🚉 On Trains: Post #4"

[taxonomies]
categories = ["Random"]
tags = ["On Trains"]
+++

This is going to be a slightly long introduction, but bear with me.
Let's imagine that you're researching existing tools for a project of yours. [A university project, perhaps](/posts/ontrains-0). Since this field is slightly new to you, and overall pretty niche, you need to figure out what exists and what you need to implement from scratch.

You've got quite good at searching for new stuff, and getting results that balance finding new tools for things you didn't know existed (including problems you weren't aware of) and tools that are completely unrelated to what you do (like a hyper-optimized tool for data from 50 years ago about a city in Iceland).

You click on many links in different tabs. Scroll through and close the ones that are clearly not what you do. And end up with a shorter list of projects that you aren't quite sure what they do.

But what exactly are the heuristics to quickly evaluate whether the projects are relevant, and get them up and running?

## Open trip planner
As mentioned [a few weeks ago](/posts/ontrains-1), I stumbled upon this piece of software called Open Trip Planner. My research is related to trips and planning, so it obviously piqued my interest. It has 2.2k stars on GitHub, which makes it relevant enough in this software niche. And the last commit was made a few days ago. Ticks all the boxes so far.

My monkey brain scrolls through the repository and website, looking for images or big text to understand what I can look like.

Ugh. The README file looks ugly. Why? No images, no "Quick Start" section. Ugh, all code is written in Java. Whatever, that's just personal preference. Their website also doesn't say much. "OpenTripPlanner - Multimodal Trip Planning". Links back to the codebase and docs, with no images, and instructions on how to run and optimize parameters for a tool I don't yet know what is about. Fine, I guess I'll actually read what they wrote in the README. [RTFM](https://en.wiktionary.org/wiki/RTFM).

```
OpenTripPlanner (OTP) is an open source multi-modal trip planner, focusing on travel by scheduled public transportation in combination with bicycling, walking, and mobility services including bike share and ride hailing.
```
Okay, good, but what exactly does it do?
```
Its server component runs on any platform with a Java virtual machine ( including Linux, Mac, and Windows). It exposes GraphQL APIs that can be accessed by various clients including open source Javascript components and native mobile applications.
```
Okay, great, but what exactly does it do?
```
It builds its representation of the transportation network from open data in open standard file formats (primarily GTFS and OpenStreetMap).
```
TO DO WHAT?

Maybe it's me. It's probably quite evident. It's a trip planner. You have a trip, and it plans it. So I can just query it, saying, "I want to go from point A to point B tomorrow at noon," and it returns me the options. Surely. Right? Or?

Let's find some examples, I'm sure that's the next section.

```md
## Performance Test
📊 Dashboard

We run a speed test (included in the code) to measure the performance for every PR merged into OTP.
```
Oh.

I'm sure you're very proud of your performance. I just don't know of what, really. 
```
More information about how to set up and run it.
```
YES YES YES! Oh. It's on how to set up the performance test. Great. I still don't know what the tool is and how to set it up, but I know have instructions on how to set up the performance tests.

I'm not going to go through the whole README here. But basically, the other sections are:
- Repository layout
- Development
- Getting in touch

There's a link to the main documentation in "Repository layout".

### Setting it up
Alright, I guess I'll learn what it's about by running it and looking at the interface (if there is any). So, the docs.
```
As of 2020, the codebase has been in active development for over ten years, and is relied upon by transportation authorities and travel planning applications in deployments around the world.
```
Okay, we're getting a bit more information. It's not exactly clear what they use it for, and it's 2024, but it seems like it's useful for transportation authorities and travel planning. Cool.

The `Versions of this documentation` section takes most of the page, hiding the slightly more useful sections underneath: `Audience`, `Quick Start`, `Getting help` and `Financial and In-Kind Support`.

```md
## Audience
The end users of OTP are the millions of people who rely on it to help plan their daily travel, often without even knowing they are using OTP. As an infrastructure component, installation and configuration of OTP tends to be somewhat technical and essentially invisible to those end users.
## Quick Start
We encourage you to read the introductory sections of this documentation to familiarize yourself with OpenTripPlanner use cases and configuration. But if you want to get started right away running your own OTP instance, the best place to start is the Basic Tutorial page.
## Getting help
The fastest way to get help is to use our Gitter chat room where most of the core developers are.
```
Remember, I still don't really know what the software does. Let me click on the Basic Tutorial page. Woah. A long list of commands to execute, things to install and run.

I'm not quite ready to commit to installing tons of things and spend hours debugging for a tool I don't yet know whether I will need it or not.

Oh! `Container image`. I like that. I can then run their Docker image, one command, simple enough. I will even run it on my sandbox server, which I use for anything that can break at any point.

`Exception in thread "main" java.lang.OutOfMemoryError: Java heap space: failed reallocation of scalar replaced objects` Hmmm. That's not good. Maybe the data they use by default is not good enough? It's supposed to be only for Berlin. The scope of my thesis is all of Europe. That's not a good start. Let's try their other example, Portland. Ah, same. 

### Figuring out my monkey brain needs to chill
In OTP's defence, they do actually have a section on their wiki that answers most questions I needed to know. It's clear, well written, and honestly not that hidden. It does however lack proper set up instructions, and more importantly, it lacks links pointing to it from other obvious places!

## What I needed
Complaining is nice. But how can it become useful?

Here's what I needed to know:   
- What does the software do?
- Who is it for/why was it created?
- (How) can I run it?
- What are known limitations?
- State of the project/is it maintained?

An ideal README file would therefore look like this:
```md
# Project name - short project description
![screenshot of the interface]

Project name is a [what it is] designed for [task or audience].
It was developed by [organisation/person] as a [reason for development] in [date]. As of [date], it is [state of the development: maintained, abandoned].

## Quickstart
`[few line commands that can run on any machine, with demo data if necessary]`
This command will [explain what it does].
## Next steps
The tool is structure this way: [...]
Check out the [wiki, documentation, reference] for more information.

## [Whatever you want: contribution guidelines, troubleshooting, licence ...]
```
And that's it! The above felt too serious, so I decided to apply it to an imaginary project:

```md
# HearMeOut - share your ideas to the right person
![/images/dashboard.png]

HearMeOut is a person search engine and provides a simple interface designed to share your crazy idea with the relevant person. After setting up a mail server, simply type the idea on the webapp or CLI and HearMeOut will find the relevant CEO, politician, or other person to send it to.
It was developed as a side hobby in 2024. This project is unmaintained.

## Quickstart
After cloning the repository and installing the dependencies in requirements.txt, run `python setupmailserver.py` to set up a mail server.

After that, executing `python hearmeout.py --idea "A Yoga carriage for Deutsche Bahn trains"` will initiate a search and most likely email the CEO of Deutsche Bahn with the message "Hear me out: a yoga carriage for your trains".

*Warning*: you might want to use the `--confirm` flag, to ask for confirmation before sending the email.

## Next steps
You can find more information in [the wiki]
```

## Closing thoughts
> If you're reading this when it came out (or %365 days later), Happy New Year! I wish you a year full of kindness and openness unlike some statements written above. In my defence, most of this blog was written last week, in 2024, and I really hope this post sparked more amusement than anger.

I have always been fascinated by blogs or video essays about user experience and how to improve it. I'm thinking of [tonky's blog](https://tonsky.me/blog/centering/), or [Tantacrul's videos](https://www.youtube.com/watch?v=4hZxo96x48A).

I always felt that these contents are both very entertaining and educational at the same time. One example is of course Martin Keary (Tantacrul), becoming the Product Owner at MuseScore after making a video criticising their design and suggesting better options (link above).

My hope is that this wasn't perceived as a long rant about meaningless problems, but a more constructive (and entertaining) feedback to anyone trying to make an open source project (including myself).

As a side note, I really hope this won't discourage anyone to make their code open source. Please share it, even if your README isn't perfect! Open source code with a bad documentation is still better than closed source code :).

This blogpost was also useful for taming my monkey brain and trying to have a more organised approach to discovering a new tool, without skipping steps.

Oh, and if you were wondering, I will probably not use OTP in my master thesis, as in they decided to change their scope in version 2.0 to become less research oriented. Since there are other options other than using their unmaintained 1.5 version, I decided to go for those.
*Note: no text in this document has been generated or rewritten by a Large Language Model.*
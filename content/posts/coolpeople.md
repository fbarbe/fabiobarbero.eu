+++
title = "😎 Cool websites of (cool) people I've met"
date = 2021-04-15
description="A non-exaustive list of websites of cool people I've met."

[taxonomies]
categories = ["Random"]
tags = ["Challenge", "List"]
+++

Here is a list of cool people I've met in the past few years. If you have some spare time, I'd recommend looking at their projects on their websites and showing some support. Please note that the list has no particular order.

## Paulius Skaisgiris
Paul has been my flatmate for two years and I have shared [many projects](https://skaisg.eu/portfolio/) with him.
You can follow him on [his personal website](https://skaisg.eu/), or "slice of cerebral brain" as he calls it. I will convince him to write more blogposts there.

## Krzysztof Cybulski 
My second flatmate during my bachelor years, Chris, also has a [cool personal website!](https://kcyb.eu/).
He has a passion for pure mathematics and Neural Network, and tries to somehow merge the two in his research.

## Tommi Boom
This italian fellow has perhaps the most active website of all the ones listed here. 
You can find many of his projects on [his personal website](https://tommi.space/). 

When adding this website to the list I explored it a bit more and found out that he had been doing a lot of stuff! Definitely worth checking it out.

_You can find me on [this blog post](https://tommi.space/vivi-la-vita#!) (I'm Fabio from Brussels!)_

## Matteo Prock (Canvai)
If you like electronic music, you definitely want to check out [Canvai's music](https://www.youtube.com/c/Canvai/videos). Matteo also has a [personal website](https://www.matteoprock.com/), where he lists a bunch of other projects he has done. I was lucky enough to see/test his [Tapescape](https://mortenblaa.itch.io/tapescape) while in development!

## Moritz and Simone
These two young lads took their relationShip to the next level and bought a second hand boat to travel around Europe for a year.

You can find them on [their blog](https://holothuria.de), which I will also follow with great interest. I helped design part of their website, such as [their 404 page](https://holothuria.de/404.html).

## Walter Simoncini
Walter and I have also shared multiple projects throughout the year. He doesn't have much stuff on [his website](https://ashita.nl/) yet, but you can check out his [meme generator](https://meme.ashita.nl/), hosted on our shared VPS server.

## Cameron Browne
Cameron has been an amazing thesis supervisor and is widely known for publishing the first machine generated board game, [Yavalath](http://cambolbro.com/games/yavalath/) and creating the [Ludii General Game System](https://ludii.games/).
[His personal website](http://cambolbro.com) contains his really cool projects, mainly related to board games.

## Lucas-Andreï Thil
[Lucas](https://lucasandrei.com/) is another good friend from university. Although I may not share some of his (perhaps past) interests such as quantum blockchain (whatever that is) he seems to be putting a lot of high quality articles on his website full of his unstoppable passion to learn and do.

I'm also very happy to [appear](https://lucasandrei.com/pages/cool_sites.html) on his website :).

## Leonardo Venturi
[Leonardo Venturi](https://www.leonardoventuri.live/) is an amazing actor and performer I met at the [Scambi](https://scambi.org) festival. He has a great energy a very positive vibe.

Update: his website seems to be down, but you can still find it on the [Internet Wayback Machine](https://web.archive.org/web/*/leonardoventuri.live)

## Jakob Raith
[Jakob Raith](https://doingitraith.net) was one of the amazing people I met during my Erasmus in Copenhagen. You can find and play some of the cool games he developed on his website!

## Stephen Fay
I met [Stephen Fay](https://stephenfay.xyz/) at my high school orchestra in Brussels. He was a really good cello player. Unfortunately tho, you won't find anything cello-related on his website: he has a lot of cool compsci projects, and interesting articles!

## Giacomo Moroni
Giacomo is currently the drummer of the band [The Blind Monkeys](http://theblindmonkeysband.com/).

## You?
_Please let me know if I forgot anyone! I'd be happy to add you to the list_
+++
title = "📬 Replying to Academia SPAM"
date = 2024-06-24
description="Given your innovative work in this field, we would be honored to have your expertise showcased in our journal."

[taxonomies]
categories = ["Random"]
tags = ["Academia", "Recommended Posts"]
+++

# I replied to Academia SPAM
... and got a nonsensical paper published.
## Part 1: Context
I am not a Professor or a Doctor of any kind. My [few publications](https://fabiobarbero.eu/portfolio/) have not had a big impact in my field, and are really not that innovative or revolutionary.

And yet, multiple times a year, I receive emails that look like this:

<blockquote>
From: venus.loh@acad-pub.net

Subject: Invitation to ΡuЬlish Your Аrtiϲlеs

Dear Prof. Fabio Barbero

Hope my email finds you well!  
 
I am the managing editor of Information System and Smart City , and we in the academic community greatly respect and appreciate your outstanding contributions to this field. Our new issue has received 30 articles so far and 6 articles have been posted on the journal's website.  

We believe that your article will definitely bring new thinking and inspiration to scholars in this field. As an esteemed scholar in the field, we would like to invite you to contribute your latest research to our journal. Could you please let us know your decision on whether you have a plan to contribute one research paper, review article, letters, editorials, commentaries, perspectives, case report or communication to this journal? We are pleased to inform you that if you submit your article before  31 May  , We will promise to publish your article for free.  

If you or your excellent students and colleagues have papers in hand, you could send them directly to me, and then I will send them for peer review. Our publication process and thorough peer review ensure the timely and high-quality dissemination of your research findings. When it's published, I will send you the DOI code of your article.  

</blockquote>

Now. There are many reasons why I usually automatically delete or ignore these emails. They usually fall into two categories:
- Content:
    - I am not a professor or "an esteemed scholar in the field"
    - I have never worked in the field of "Information System and Smart City" (or any other buzz-wordy topic - trust me, they can get creative)
    - The domain used to send the email from links to an empty or unrelated page (acad-pub.net currently claims to be the homepage of the Koror State Government)
- Formatting:
    - The email is full of spelling mistakes or weird formatting
    - The subject contains non-ASCII characters (the "b" in ΡuЬlish), probably to escape SPAM filters
    - My name in the email is usually in a different font/formatting, likely copied and pasted from somewhere else

Seriously - "the academic community greatly respect and appreciate your outstanding contributions to this field"?

The fact that they found my email address is not that weird: scraping academic websites is not a hard task, and emails are always easy to get from publications. Some of the emails even contain the name of one of my publications, saying something like "this paper made a great impact in the field" (it hasn't).

But unlike an email from a rich prince who is trying to give me all his money and all he needs is my credit card info, these academia emails always intrigued me. If you're trying to scam someone, would you really target educated people who have published papers? What are they trying to get from me?


Sooo.
I've started replying to those spam academia emails.

## Part 2: Setup
This was of course inspired by [probably my favourite TED Talk of all time](https://www.ted.com/talks/james_veitch_this_is_what_happens_when_you_reply_to_spam_email).


The plan was simple. Create a fake paper, with bogus content. Create a fake persona, who's a scholar trying to publish it, and reply to these emails from a new email address.

Meet Anne Throapologius.

| | Name | Institute | Field | Paper |
| --- | --- | --- | --- | --- |
| ![Anne's cartoon picture](/images/academiaspam/anne.png)| Anne Throapologius | Biocomputing Laboratory for Organized Research and Practical Studies (BLORPS) | Anthropology, Quantum Computing | "Quantum Computing in Anthropological Research: An Interdisciplinary Approach" |

She is really excited about her 4 pages paper titled "Quantum Computing in Anthropological Research: An Interdisciplinary Approach", with nice plots and references, written at the "Biocomputing Laboratory for Organized Research and Practical Studies", or BLORPS for short.

To make sure her publication is a success, Anne decided to check out and reply to every flattering email pointing out how great of a scientist she is. 

We'll come back to Anne's quest to publish a paper later, but first, let's actually read these emails and see what they're trying to get from you (spoiler: they want your money).

## Part 3: What do these SPAMs really want?

Some of these Academia SPAM emails were more straightforward about what they wanted than others. 

Take this email for example:
<blockquote>
Happy New Year! Everything goes well with you.  
  
Thank you for reading the paper invitation from our journal /Journal of Autonomous Intelligence/ (Citescore 1.1), which has been indexed in Scopus of the category of Computer Science. JAI is a peer-reviewed interdisciplinary journal that publishes original research and review articles covering all aspects of autonomous intelligence with emphasis on artificial intelligence and robotic self-learning.  
  
Given your innovative work in this field, we would be honored to have your expertise showcased in our journal.  
  
Submitting your paper to our journal will provide an excellent platform for sharing your research findings with a wide audience of scholars, researchers, and practitioners in the field of AI. We sincerely hope that you will consider submitting your next paper to this journal. 

The Article Processing Charges (APC) is 1500 USD.
</blockquote>

"Everything goes well with you" is competing for the best sentence I've ever got in an email, especially before asking me for 1500 USD to publish my non-existing paper in Autonomous Intelligence.

A big chunk of academia SPAM is like this, either being very upfront about how much you need to pay for it or with more sneaky ways, like saying "we created an account for you on this platform, check it out":

<blockquote>
Subject: Activate your account for the Heliyon Editorial Manager Site

I would like to inform you that your name and email address have been added to Editorial Manager, the online submission and peer review tracking system for Heliyon. This was done for one the following reason(s):  
  
*You have been selected as a potential reviewer for future submissions (a separate review invitation will be sent once you have been selected to review a submission).  
*You authored a submission that was transferred to this journal from another journal.  
*The journal would like to invite you to contribute to a special issue.  
*You have recently been added to the Editorial Board of the journal.
</blockquote>

On these websites, such as hxxps://www.sciencepg.com/information/[^1] prepay, you are then told that you need to pay hundreds or thousands of dollars to publish your paper in this "prestigious" journal (which you've never heard of).
Some of them even have "special discounts" for a limited amount of time:
![](/images/academiaspam/publishingdeal.png)

Can't wait to get a special "publish TWO (2!) papers and get a THIRD (3rd!) paper published for FREE!!!" deal.


So yeah, we seem to have figured it out. They are just (semi-)legitimate publishing companies that make a business out of flattering scholars, making them think they're getting the unique opportunity to publish in a prestigious journal, and asking them to pay for it.

But wait - what about the email I displayed at the beginning of this article? They are promising - yes, promising! - to publish my article for free if I submit it before the 31st of May.

So I did. Well, Anne did. Remember her?

## Part 4: Quantum computing for Anthropology
<blockquote>
Hi,

You will find attached my recent paper on Quantum Computing in Anthropological Research: An Interdisciplinary Approach.

Looking forward to hearing from you soon!

Best,
Anne
</blockquote>

Anne was very excited to send this email. It was her first time contacting a journal. In fact, it was even the first email she sent from her email account!

Her paper on using Quantum Computing in Anthropology is unconventional, for sure, but she is certain it will make a groundbreaking impact in this field.

She made sure to send it to two different invitations she got, one from `venus.loh@acad-pub.net` and another from `maggie.cheung@ep-pub.net`.

Venus introduced herself as the "managing editor of Information System and Smart City", of the "Academic Publishing Pte Ltd". Smart cities would surely benefit from more anthropological studies made with Quantum Computing.

Maggie is the "Editor-in-Chief" at JIPD. She had sent her an email saying that "JIPD received a CiteScore of 1.6 in 2022, ranking 204/502 (Q2) in social science. In the meantime, [they] got [their] first Impact Factor of 0.7, released by Clarivate in JCR.". Anne doesn't know what any of these words mean, but she's amazed by this long list of things that sound important. 


A few days later, she finally gets a response! It's from Maggie:
<blockquote>
Dear Prof. Throapologius,
 
 Many thanks for your kind reply. We appreciate your willingness to participate in our future events. It could be much more helpful to the development of our journal. At the same time, we could make your paper more impactful.
 
 In order to follow your paper better, could you please send us the Word version of your paper? And please keep your manuscript no less than 5000 words. 
 
 Looking forward to hearing from you. 
 
 Warm regards, 
 
 Miss Maggie Cheung
</blockquote>

"Aha! They're testing me!" - thinks Anne, who knows that no tech conference would ever seriously ask for a Word version of the paper, since pretty much every paper is written with LaTeX nowadays.

Anne replies saying that she naturally wrote her paper in LaTeX and sends the original TeX file, with all the necessary stuff zipped.

Venus Loh, on the other hand, sent her a link to a platform where she had uploaded her paper:
```
hxxps://ojs.acad-pub.com/index.php/ISSC/authorDashboard/submission/1383
username:athroapologius

password: 123456
```
"123456" as a default password, seriously? Anne decides not to tell her friend about this, for fear they would write a script trying all the author names with that password on the website, and shut the whole thing down. After all, she really wants this paper to be published, and she's sure that there is a very strong password generator that just happened to have generated this password by chance.

On the login page, she sees the face of Prof. Saeid Eslamian, who is the "Editor-in-Chief". This guy has [a Wikipedia page](https://en.wikipedia.org/wiki/Saeid_Eslamian), so he must be important!

The next email from Venus got her really excited:
<blockquote>
Your article ID: 1383 has been reviewed and this is the review opinion, you can refer to it for modification, please send it to us after the modification is completed, we will send it to the typesetting department for typesetting online, if you do not need to modify it, please let us know that your article meets our requirements for receiving manuscripts, or you can send it to the typesetting department for direct online.
</blockquote>

Attached to the email was a Word document [^2], which contained the following review (extract):
```
1. How do you rate the significance of the research (in a scale of 1 to 5 with 5 being the most significant)?  3

2. How do you rate the originality (in a scale of 1 to 5 with 5 being the highest)? 4

3. How do you rate the experimental design and quality of data (in a scale of 1 to 5 with 5 being the highest)? 4

Suggested Revision: Provide a more detailed explanation of the data sources, including their origins, the type of data collected, and any preprocessing steps undertaken before applying the quantum algorithms.
```
Anne would have expected her paper to rank higher on the "originality" scale, but was overall happy with the feedback. After all, it was her first paper! She sent an updated version of the paper (in LaTeX, again), and waited some more. The next email from Venus surprised her even more: it contained a Word version of her paper, with feedback written as Word comments. Furthermore, all figures in the paper had been replaced with lower-quality screenshots. "This must be a sign of high dedication from the team" - thought Anne.

She answered and improved the few comments asking her to cite figures in text (oups), and sent it back.

A few weeks later, Anne's excitement reached its peak when an email landed in her inbox with the link to her published paper! The email asked her to share it as much as she could, so here it is: hxxps://ojs.acad-pub.com/index.php/ISSC/article/view/1383 [^1] [^3].

What a success!

## Further investigation about these "journals"
When Anne's paper got published, I was really confused. I hadn't been asked to pay anything to publish. The paper is freely available. What's their business model?

I thought that by answering all emails and attempting to publish a paper I'd be able to understand what these badly-written emails really wanted. And yet I know nothing more than when I started.

Well, not quite. I decided to contact all authors that published on acad-pub.com (their email was available in the paper) and ask them how they got their paper published and whether they knew anything more. As Anne, I also contacted the spammers asking for more information. More on that further down.

### Journals that are straightforward about wanting money:
Here's some journals that have sent me emails I'd consider SPAM:
- hxxps://systems.enpress-publisher.com/index.php/jipd/about/submissions#onlineSubmissions (Article Processing Charges (APCs)(hxxps://systems.enpress-publisher.com/index.php/jipd/about/editorialPolicies#custom-4: US＄1200 - Notice: The APC will be US $1800 for each manuscript submitted after July 1, 2024.)
- hxxps://www.sciencepg.com/information/prepay

### acad-pub.net
I made a small python script to look at the time the emails were sent at per domain, and both acad-pub.net seemed to be on UTC+08:00 (which is Singapour), with emails being sent from 09:10 to 17:26 (which are standard office hours).

#### Website
acad-pub.net: Used to be a [nginx setup page](https://web.archive.org/web/20240514142328/http://acad-pub.net/), is now a [non-filled homepage of the "Koror State Government"](https://web.archive.org/web/20240614155323/http://acad-pub.net/) (I love you, Internet Archive 💕. If you can, please [donate to them!](https://archive.org/donate))

acad-pub.com: This seems to be the proper front page of the company. They seem to have put the same amount of effort in their website as in their grammar in the emails they sent: buttons that lead nowhere or to the same page, slow loading times...



#### Company
Both the emails received and hxxps://acad-pub.com/ [^1] give us the following information:

<blockquote>
Academic Publishing Pte Ltd
73 upper Paya Lebar road #07-02B-01 centro bianco Singapore 534818
Tel：+65 83184869
Email：editorial_office@acad-pub.com 
</blockquote>

Looking at the [Centro Blanco on Google Maps](https://maps.app.goo.gl/6Hw76vxLMg6eTUaC8) it seems to be a building with a lot of offices, with one of the reviews stating "SocialEpoch is working here, and they scam and fraud people, so take care!". Another Google Maps review is an image of a random indian-looking guy (I hope it's not doxxing?).

They have a [LinkedIn](https://sg.linkedin.com/company/academic-publisher) profile.

#### Prof. Saeid Eslamian
"Prof. Saeid Eslamian" is the only name and face you see associated to this company on the whole website. The website claims he is the "Editor-In-Chief".

[His Wikipedia page](https://en.wikipedia.org/wiki/Saeid_Eslamian), which is marked as "an autobiography" and "written like a résumé" since 2013, claims that he has been a professor at many prestigious university, such as McGill and ETH, but doesn't cite relevant sources.

The Wikipedia page also says "He is now the Director of Excellence in risk management and natural hazards.", without saying at which organisation, and links [his LinkedIn](http://ca.linkedin.com/in/profsaeideslamian) as his personal website.

I have contacted him on LinkedIn, asking whether he was affiliated to the website and warning about the possible misuse of his image.

At the same time, I contacted McGill, ETH and UNSW asking them to confirm whether or not he had worked there. McGill and ETH responded that information about past lecturers cannot be verified without the individual's consent, while UNSW did not reply.

Princeton has [a page for a staff member called Eslamian, Sayed](https://findingaids.princeton.edu/catalog/AC107-03_c5706) (notice the y instead of the i), which I am guessing could be a different transliteration of an Arabic name, while the Isfahan University of Technology has [a page for him](https://eslamian.iut.ac.ir/).

#### Business model
Honestly, I don't know. Their website says there is a [800$ fee](hxxps://ojs.acad-pub.com/index.php/ISSC/APC) for publishing. So I contacted all the 18 emails of the authors in the papers published on their website. Two emails bounced back, and 3 people replied, saying that they had been contacted by email and had not paid any fees as well.

The earliest publication on the website is from November 2023. How have they been running this organisation since?

There is clearly something fishy going on. Everyone who replied to my email and provided the mail that contacted them was contacted by a different person/email address at acad-pub.net. This list includes "ailill.wong" and "nadia.joseph".

### ep-pub.net
Their emails were often particularly badly translated, and once contained chinese characters: 于2024年6月19日周三 10:32写道 ("On Wednesday, June 19, 2024 at 10:32 pm wrote"). For this reason, I started investigating whether acad-pub and ep-pub where in fact the same entity based in Singapoure, by asking them directly by email. 

I got the following response from acad-pub:
<blockquote>
Dear Prof. Anne Throapologius
Thank you for your support.
We are a new journal, but we have developed well in the past two years, and can be searched by Google Scholar, articles can also be, our publishing house plans to send the journal to SCOUPS for review in the second half of this year.
Secondly, ep-pub.net is our colleague, this is Ember Press, we are an academic press. The journals of the two publishers are different, but we are also connected.
Again, thank you for your support. If you have any questions, please feel free to contact us.

If you have new articles or your colleagues and friends have articles to submit, we are also willing to publish for you and your friends free of charge.

Looking forward to our next cooperation!
</blockquote>

So yes, the two domains are related.
Looking at their email footer, I could also see that they use [Mailsuite](https://mailsuite.com/en/), a mass mail marketing platform.
#### Reviewing other people's papers:
On my student email, I got this email from `nadia.joseph@ep-pub.net`:
<blockquote>
Dear Prof./Dr.Barbero,Fabio

Hope everything is going well with you.

Considering your great achievements in the field, we sincerely be grateful that you would serve as an excellent reviewer of the manuscript, “Multi-Engine and Multi-Channel Sentiment Analysis for Enhanced Digital Reputation Management**”**, which has been submitted to our open access journal ISSC . The submission's abstract and keywords are inserted below, and we'll appreciate it if you will consider undertaking this important task for us.

If you are willing to accept this invitation, I will send you the full text later. Thank you very much for considering this request.

And there will be waive APC if you submit your article to our journal since you kindly spent time reviewing the manuscript. We look forward to working with you more.
</blockquote>

I got similar emails from acad-pub.net.

This probably means that reviewers are just random students working for free from their good heart!

### Connections with other journal
I agreed to review someone else's paper as Anne. The paper I was sent was for yet another journal, systems.enpress-publisher.com .

This is yet another puzzle. From what we've seen before, ep-pub and acad-pub are linked, and these are also linked to enpress-publisher.com.

How big is this academia spam network? Why are they all linked? Why is acad-pub.net the website of the "Koror State Government"?
I'm afraid I don't have answers to these questions.

## Conclusion

Are these journals straight-out scams? That is arguable. They do seem to be having some pretty dishonest ways of getting you to publish with them: spamming tons of scholars, trying to flatter them with compliments and boasting about being a reputable journal (I have not checked whether their claims of success - such as "Citescore" - are true, and to be fair I don't really know what it means either). But at the same time, some of them might actually be legitimate journals with very shady tactics, so I would not be comfortable calling them scams. Some of them seem to be connected in a weird big network, each with a different title and image, which is rather odd. In any case, I would of course not recommend publishing with any of these journals.

This ended up being a big rabbit hole. I decided to publish this blogpost now, but honestly I could keep researching this for a few more months.

If you are interested in digging deeper, here's a few things to look for:
- How did acad-pub.com get a doi.org link for their papers?
- How is Prof. Saeid Eslamian involved in this?
- How many of these platforms are linked with each other?
- What is their business model, really?


If you're interested to know more about this, a friend recommended me to look into [research paper mills](https://en.wikipedia.org/wiki/Research_paper_mill).


Thank you for reading :). This has been quite a journey for me, and I'm quite happy with this blogpost that came out of it. Always feel free to send me a message about this!

## UPDATE: 02/07/2024
There is a name for these kinds of organisations! They are called [Predatory Publishing](https://en.wikipedia.org/wiki/Predatory_publishing)

Here's how I found out about it: I got one of those usual emails from `daisy.tan@acad-pub.net`. The email had a different address at the bottom:

```
Journal of Infrastructure, Policy, and Development Indexed by Scopus and ESCI
EnPress Publisher LLC. 9650 Telstar Avenue，Unit A，Suit 121，El Monte，CA 91731, USA.
```

On Google Maps, this has one 1-star review from "Donald Dump", that says "Predatory publishing is a no" (thank you, Donald).
I then looked up "Predatory Publishing" and found that it was exactly what I had been dealing with. The website https://thinkchecksubmit.org/ "helps researchers identify trusted journals and publishers for their research.". Great stuff.

I am surprised that neither I nor my academic peers have ever heard about this before. It would be nice to improve awareness of this topic!

Regarding Sayyed Saeid Eslamian, the University of New South Wales confirmed that he completed his PhD in the School of Civil and Environmental Engineering, University of New South Wales in 1998.

## Notes
[^1]: All https urls from sketchy websites have been replaced to "hxxps", to avoid accidental clicking and giving them Search Engine Optimization points.

[^2]: I have scanned all attachments for viruses (no virus was detected) and have taken proper measures to avoid getting a virus on my computer (kinda). Please never open attachments from shady emails kids.

[^3]: At the time of writing, this link seems safe. It does not even contain ads or tracker (according to Ublock origin), and has a valid https certificate.

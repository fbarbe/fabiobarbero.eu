#!/bin/sh
zenity --help > /dev/null;
if [ $? -ne 0 ]; then
    echo "Zenity is not installed. Please install it: https://help.gnome.org/users/zenity/stable/"
    exit 1;
fi
java --version > /dev/null;
if [ $? -ne 0 ]; then
    echo "Java is not installed. Please install it at https://www.java.com/en/download/";
    exit 1;
fi
curl --version > /dev/null;
if [ $? -ne 0 ]; then
    echo "curl is not installed. Please install it.";
    exit 1;
fi
INSTALL_PATH="$HOME/Games/Ludii/"; #where all the Ludii data will be installed
echo "Ludii will be installed at $INSTALL_PATH";
mkdir -p $INSTALL_PATH; #only make directory if doesn't exist

LUDII_LAUNCH_SCRIPT="#\!/bin/sh\nLUDII_PATH=$INSTALL_PATH\nLUDII_SERVER_VERSION=\$(curl -s -N https://ludii.games/download.php | grep -o -m 1 \"Ludii.*\.jar\")\nLUDII_LOCAL_VERSION=\$(ls \$LUDII_PATH | grep -o -m 1 \"Ludii.*\.jar\")\nif [ \"\${LUDII_SERVER_VERSION}\" = \"\" ]; then\n    echo \"Unable to determine latest Ludii version\"\nelse if [ \"\${LUDII_SERVER_VERSION}\" != \"\${LUDII_LOCAL_VERSION}\" ]; then\n    echo \"Found newer version of Ludii: \$LUDII_SERVER_VERSION\";\n    echo \"Downloading newer version...\"\n    wget --progress=bar:force \"https://ludii.games/downloads/\$LUDII_SERVER_VERSION\" -P \$LUDII_PATH | zenity --title=\"Ludii Updater\" --progress --text=\"Downloading \$LUDII_SERVER_VERSION\" --auto-close;\n    if [ \$? -eq 0 ]; then\n        echo \"Removing old version...\"\n        rm -f \"\$LUDII_PATH\$LUDII_LOCAL_VERSION\";\n    else\n        echo \"Unable to retrieve from server (code \$?)\"\n    fi\nelse\n    echo \"Your version is up to date\"\nfi\nfi\nLUDII_LOCAL_VERSION=\$(ls \$LUDII_PATH | grep -o -m 1 \"Ludii.*\.jar\")\nif [ \"\${LUDII_LOCAL_VERSION}\" != \"\" ]; then\n    echo \"Launching \$LUDII_LOCAL_VERSION\"\n    chmod +x \"\$LUDII_PATH\$LUDII_LOCAL_VERSION\";\n    cd \$LUDII_PATH;\n    errors=\$(java -jar \"\$LUDII_LOCAL_VERSION\" 2>&1 > /dev/null &) &\n pid=\$!;\n sleep 1; ps -p \$pid;\n    if [ \$? -ne 0 ]; then\n rm -f \"\$LUDII_LOCAL_VERSION\"; zenity --error --text=\"Error running jar file. Try re-running the app\"; fi\nelse\n    echo \"No ludii file found\";\n    zenity --error --text=\"No Ludii jar file found\";\nfi\n";
LUDII_APPLICATION="[Desktop Entry]\nType=Application\nExec=sh \"${INSTALL_PATH}Ludii.sh\"\nStartupNotify=true\nGenericName=Ludii\nIcon=${INSTALL_PATH}ludii.png\nName=Ludii";
cd $INSTALL_PATH;

echo $LUDII_LAUNCH_SCRIPT > Ludii.sh;
chmod +x Ludii.sh;

curl -s https://ludii.games/forums/images/ludii_logo.png > ludii.png;
echo "Password required to install Ludii as a desktop application";
echo $LUDII_APPLICATION | sudo tee /usr/share/applications/Ludii.desktop > /dev/null;
echo "Ludii is now installed. You can now select it from the Applications menu.";
+++
title = "About"
description = "Fabio Barbero"
+++

Hey! My name is Fabio :). I'm currently completing my Master’s degree in [Data Science for Decision Making](https://curriculum.maastrichtuniversity.nl/education/partner-program-master/data-science-decision-making) at Maastricht University.

I have extensive experience in tech, specializing in Artificial Intelligence (Computer Vision, Mathematical Models), Data Science (such as Social Media Data), and Software Development (Full Stack). In my free time, I like running small LLMs on my CPU and reverse engineering software — particularly web-based platforms. I've proudly discovered a few small vulnerabilities, which I may discuss [on my blog](/tags/recommended-posts) in the future.

If you're interested in my career path so far, you can have a look at my [📝 Curriculum Vitae](/CV.pdf) (Last updated on 2024-09-25).

Outside of the tech world, I’m passionate about staying active — whether through running, exploring the outdoors, or immersing myself in international environments. I enjoy learning new languages, reading, and delving into topics that focus outside of a computer screen: creativity, sustainability, healing, societal impact, simplicity, and human connection. Feel free to reach out — I’m always up for a chat!

I’ve lived in 🇧🇪 Brussels, 🇳🇱 Maastricht, 🇩🇰 Copenhagen, 🇧🇪 Ottignies, and 🇨🇦 Montreal. You’ll often find me travelling by train, visiting friends around Europe.

Music is another passion of mine! I play the piano (pretty decently), saxophone (alright), and some bass and guitar (so-so). I occasionally make music, mostly contemporary jazz, and enjoy improvising.

You can find a list of my publications and projects in my [Portfolio](/portfolio).  
_You can also [chat 💬 with my CV!](/interactive/cv). Or again, here's my [regular CV](/CV.pdf)_

If you have any question, [send me an email!](javascript:linkTo_UnCryptToMail('nbjmup;ijAgbcjpcbscfsp/fv');)
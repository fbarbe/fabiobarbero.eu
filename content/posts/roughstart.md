+++
title = "🏋️‍♂️ Rough start at my internship"
date = 2024-03-11
description="A diary entry about frustration on working when things don't work."

[taxonomies]
categories = ["Growth"]
tags = ["Work", "Stream of thoughts"]
+++
```
- What are you doing in Montreal?
- I'm doing an internship at an AI Startup
- Oh cool! What are you working on?
- Oh, I use computer vision to detect athletes and extract metrics from it
```
Yeah, I wish.
## Fixing the freaking computer
So, I might have accidentally broken the ZED Box Orin NX computer on the first day of my internship. Great. It's not like it's more than 1000 bucks and I literally can't do anything without it. In my defence, the support team did say that if you upgrade Ubuntu on it the whole thing breaks, which I think it's pretty stupid. It might be that, or the fact that I might have accidentally uninstalled python. The thing is, it's not loading the kernel (yes, I definitely know what that means) after the booting screen that says "NVIDIA". So yeah, it's nice that it turns on, but it really sucks that the freaking thing doesn't give me an error message. Like, come on, don't just hide and not do anything, show emotion! Be mad! Just tell me I'm an idiot and why it hurt you, so I know how to fix it!

Anyway.
I've spent a whole week trying to fix it. My motivation got really low. I tried their official script. Didn't work. So I emailed customer support, which is in a different time zone so takes a day to respond. They told me that it broke because I updated it and that I should try their official script. I told them "hey, my first email literally said I tried the script and it didn't work". So they told me (and this was already day 4 or something) that I need to run it from Ubuntu 20, and since I'm using Ubuntu 23 it doesn't work it. I don't believe it a second, but I can't ask for extra support unless I try it. So okay, I need to find a computer to install Ubuntu 20 on. Fine. There's this extra computer at work I've been told I can kinda use. After asking my supervisor/boss/friend-of-a-friend/friend (?), who's conveniently taken a week off to ski the first week of my internship, he's replied something like "yeah sure as long as you don't break it", so yeah I got myself a great reputation. By the way, my other "boss" (man this is weird, whatever, the other cofounder of the startup) has been ghosting me via text and didn't show up the whole week either. It's okay.
So, installing Ubuntu on the computer. How hard can it be. I've installed Linux on computers many times, from Raspberry Pis to my grandma's laptop (which she hasn't used since). It pretty much always works. But you know, there's a little chance I mess up something, like that time I accidentally formatted the Windows partition of my home computer instead of the USB stick. Oh, and installing it properly on my grandma's computer also took a week, because of those freaking nvidia drivers. But whatever. Easy peasy. So I went to buy a USB stick, apparently the lowest you can get now is 32 GB, pretty crazy - I remember buying 4GB ones, shut down and turn on the computer 3 times because I always forget which key it is to choose the boot option, do the whole install procedure to install alongside windows, and - p u f f - nope. A wild acronym appears! RST. Ah yes. Because of this wild RST, which I don't know what it is and don't really want to know (but has something to do with how the data is encoded?), I can't install Ubuntu. Fine. There's a guide to deactivate it on the official Ubuntu website.
I tried it 7 freaking times. Nope. I have changed all those registries. I have disabled it. Windows crashes. Tried to go into the recovery terminal mode. The password doesn't work. So I changed back to RST, tried to find the password saved in the computer (I was too afraid to ask my supervisor). Nope.
Well.
This was anyway only step 2 of 5, so i wanted to find another solution.
So yeah, I decided to run the script from the live version of Ubuntu 20, running from the USB stick. It's fine. It's just that it doesn't have the basic programs I need, and apt doesn't have enough storage in the default partition, so I need to link the apt cache to the other partition in the USB stick that still has 18 GB or something (thanks Stack Overflow). And now I run the recovery script.
Okay, I need to install qemu. Doesn't work. Let's look up how to install it. Thank you, reddit post named "Ok, I give up. How do I install qemu". Okay, so it's fine. Alias python to python3. Fine. I start running it, it encounters an error, I install the missing dependency, I run it again, I wait, and so on. There has to be a final error, that I can either fix or send to customer support.

So yeah, the script started running without crashing every 20min. I decided to let it run overnight at the office, expecting an error the next day. 17h later, I come back to the office to find that the script is still not done running (I'm not kidding).
hmmm. Maybe the computer suspended and it stopped without really saying? Look idk. So I'll just stop it, and start it again. In fact, let's restart the computer. Oh, the whole live environment gets reset. So I need to re-install everything. Great. So yeah, here I am, writing this diary entry while the scripts runs again.

It's really frustrating. I know it's important to overcome this kind of things, but I feel like this is maybe just not for me. I want to be outside, enjoy the beautiful sun. I'm also really good at ignoring my difficult feelings and emotions and distracting myself with these problems, which result in some pretty unhealthy mental states. I would also like to contribute to society in areas that I feel need it the most. Have contacts with people, other than "coffee" breaks (water for me). Focus on climate actions I can do. Help communities grow, and those in need. Talk to the homeless, and discover what they really need. Radical thinking of what I really need, starting from the food I eat, to the way I move my body and interact, by listening to my body's feedback through feelings and emotions.

Ah. It's difficult to think about all this. Offices give me so many uncomfortable emotions, from pressure from other peers to be productive, to feeling stuck and having to conform. Not being able to stand up and do jumping jacks every once in a while. Maybe I can, and I'm just limiting myself.

Okay, enough letting my mind run around in thoughts. Back to "working". I mean, playing chess while I wait for my script to spit another error.
I might publish this as a blogpost, I'll see how I feel. If that's the case, it could be fun to write these more often! I hope this post has entertained you and that it helps you to look at difficult situations with a grain of sarcasm and a sense of the bigger picture, as it has for me. I'd also like to say that I'm overall doing alright, and I am very grateful for what I have. Take care :).
+++
title = "🍽️ Vegan Lasagne Recipe"
date = 2021-04-26
description="Revising a great italian classic without dairy products."

[taxonomies]
categories = ["Recipe"]
tags = ["Vegan", "Recipe", "Italian"]
+++
<link rel="stylesheet" href="/styles/recipe.css">
<!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

# Vegan Lasagne Recipe
<img alt="Baked Lasagne" property="image" src="/images/veganlasagne/final_blur.png">

## Introduction
For some obscure reason recipe blogs always have a super long introduction telling the whole backstory of the recipe, so I feel obliged to have a short introduction.

Everyone knows lasagne[^1]. They're a great italian dish perfect for dates and pretty much any other occasion that involves eating. Italians tend not to like to revisit their recipes, especially when it involves using ingredients they've never heard of, such as tofu. As an Italian trying to be as vegan as possible, I have been breaking this rule and tried to replace many non-vegan ingredients with vegan alternatives. Please don't tell my grandmas.

<img alt="Baked Lasagne with flower background" style="width:100%;max-width:300px" src="/images/veganlasagne/art_final.png">

<br>
This dish was highly loved by my flatmates and both vegan and non-vegan friends, both claiming that the consistency was very much meat-like (tho you will read this from every vegan recipe, and it's rarely true. But trust me, this one is).
<br>
<img alt="Baked Lasagne on a plate" style="width:100%;max-width:300px" src="/images/veganlasagne/art_final_plate.png">


<div vocab="https://schema.org/" typeof="Recipe">

## Ingredients
<img alt="All ingredients" style="width:100%;max-width:300px" src="/images/veganlasagne/0.jpg">

- <input type="checkbox"> <span property="recipeIngredient">Lasagne pasta</span>
### For the ragu
- <input type="checkbox"> <span property="recipeIngredient">Tofu</span>
- <input type="checkbox"> <span property="recipeIngredient">Passata</span>
- <input type="checkbox"> <span property="recipeIngredient">Carrots</span>
- <input type="checkbox"> <span property="recipeIngredient">Onions</span>
- <input type="checkbox"> <span property="recipeIngredient">Vegetable stock</span>
- <input type="checkbox"> <span property="recipeIngredient">soy sauce</span>
- <input type="checkbox"> <span property="recipeIngredient">random spices</span>

### Besciamella
- <input type="checkbox"> <span property="recipeIngredient">plant based milk (soy or wheat)</span>
- <input type="checkbox"> <span property="recipeIngredient">margarine</span>
- <input type="checkbox"> <span property="recipeIngredient">flour</span>
- <input type="checkbox"> <span property="recipeIngredient">salt and pepper</span>
- <input type="checkbox"> <span property="recipeIngredient">nutmeg</span>

## Material
- whisk (for béchamel)
- oven 

## Steps
1. <span property="recipeInstructions">Scramble tofu into small pieces, and let them marinate in a bowl with soy sauce, oil and other spices. </span><a onclick="showModal('/images/veganlasagne/1.jpg', this.previousSibling.innerHTML)">[view]</a><a onclick="showModal('/images/veganlasagne/2.jpg', this.previousSibling.previousSibling.innerHTML)">[view]</a>
2. <span property="recipeInstructions">Chop the onions and carrots finely. </span><a onclick="showModal('/images/veganlasagne/3.jpg', this.previousSibling.innerHTML)">[view]</a>
3. <span property="recipeInstructions">Heat up a wok pan with some oil .</span>
4. <span property="recipeInstructions">Let tofu fry for a few minutes. </span><a onclick="showModal('/images/veganlasagne/4.jpg', this.previousSibling.innerHTML)">[view]</a>
5. <span property="recipeInstructions">Add carrots, onions and let it fry for a bit more. </span><a onclick="showModal('/images/veganlasagne/5.jpg', this.previousSibling.innerHTML)">[view]</a>
6. <span property="recipeInstructions">Add vegetable broth. </span><a onclick="showModal('/images/veganlasagne/6.jpg', this.previousSibling.innerHTML)">[view]</a>
7. <span property="recipeInstructions">Add passata bit by bit. </span><a onclick="showModal('/images/veganlasagne/7.jpg', this.previousSibling.innerHTML)">[view]</a>
8. <span property="recipeInstructions">Let it cook for around 1h, on medium heat .</span>
9. <span property="recipeInstructions">Put margarine on a different low-heated pan. </span><a onclick="showModal('/images/veganlasagne/8.jpg', this.previousSibling.innerHTML)">[view]</a>
10. <span property="recipeInstructions">Add flour, and whisk it until it becomes a solid block. </span><a onclick="showModal('/images/veganlasagne/9.jpg', this.previousSibling.innerHTML)">[view]</a><a onclick="showModal('/images/veganlasagne/10.jpg', this.previousSibling.previousSibling.innerHTML)">[view]</a>
11. <span property="recipeInstructions">Add **warm** milk to the pan, bit by bit, while whisking. </span><a onclick="showModal('/images/veganlasagne/11.jpg', this.previousSibling.innerHTML)">[view]</a>
12. <span property="recipeInstructions">Add nutmeg, salt and pepper .</span>
13. <span property="recipeInstructions">Layer the lasagne pasta in the following order: besciamella, pasta, ragu. </span><a onclick="showModal('/images/veganlasagne/12.jpg', this.previousSibling.innerHTML)">[view]</a><a onclick="showModal('/images/veganlasagne/13.jpg', this.previousSibling.previousSibling.innerHTML)">[view]</a>
14. <span property="recipeInstructions">(optional) add blended cashews with nutritional yeast (or other parmesan replacements) on top. </span><a onclick="showModal('/images/veganlasagne/14.jpg', this.previousSibling.innerHTML)">[view]</a>
15. <span property="recipeInstructions">bake for 40min. </span><a onclick="showModal('/images/veganlasagne/15.jpg', this.previousSibling.innerHTML)">[view]</a><br><br>
<i><span property="name">Vegan Lasagne</span>
By <span property="author">Fabio Barbero</span>,
<meta property="datePublished" content="2021-04-26">April 26, 2021.
<span property="description" style="display:none">An italian classic revisited with vegan ingredients</span>
<meta property="prepTime" content="PT20M">
<meta property="cookTime" content="PT2H">
<span property="recipeYield">4-6 portions</span>
<link property="suitableForDiet" href="https://schema.org/Vegan" /></i>
</div>

<img alt="Final result" style="width:100%;max-width:300px" src="/images/veganlasagne/final_transparent.png">
<img alt="Final result on a plate" style="width:100%;max-width:300px" src="/images/veganlasagne/final_plate_transparent.png">

## Notes
This recipe was highly inspired by the "original" italian recipe, which can be found on [GialloZafferano](https://www.giallozafferano.com/recipes/Lasagne-alla-Bolognese.html). If you would like to see a more detailed step-by-step guide (especially for the besciamella), you should check out that recipe.

<script>
// Get the modal
var modal = document.getElementById("myModal");
var captionText = document.getElementById("caption");

var images = document.getElementsByTagName('img');
for(var i = 0; i < images.length; i++) {
    var img = images[i];
    var modalImg = document.getElementById("img01");
    img.onclick = function(){
        showModal(this.src, this.alt);
    }
}
function showModal(src, description){
    modal.style.display = "block";
    modalImg.src = src;
    captionText.innerHTML = description;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks on <span> (x), close the modal
modal.onclick = function() {
  modal.style.display = "none";
}
</script>

[^1]: If you are confused as to why I wrote "lasagne" instead of "Lasagna", check out [this explanation](https://www.grammarly.com/blog/how-to-spell-lasagna/) (or literally any other explanation online).
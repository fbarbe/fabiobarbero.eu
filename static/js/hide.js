function hide(el){
    if(el.style.opacity == 0){
        el.style.opacity = 100;
    }
    else{
        el.style.opacity = 0;
    }
}
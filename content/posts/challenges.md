+++
title = "🔥 Challenges"
date = 2021-04-01
description="Some of the challenges I've been doing this year."

[taxonomies]
categories = ["Random"]
tags = ["Challenge", "List"]
+++

## Why
Good question. I realised a while back that I was getting stuck in many "unhealthy loops": checking my phone for no reason, browsing reddit/youtube in search of dopamine, finding myself in a stressful mindset... I had been doing meditation for a while, and finally decided to apply what I learned in those 10min meditations to my daily life.

Challenges have been a great way of getting out of my comfort zone and being more attentive to what my body feels. Waking up between 5-7am has greatly helped achieving these goals, as it gave me more time without external inputs (people don't usually text that early).

## What
### Sports
I had been doing very little to no sports in 2019-2020.
In 2021, I decided that I want to do on average 1h of "sports" a day. By "sports" I am also including my daily 30min of Yoga, very long walks/hikes. 

So far I am very much on track, even in advance! Buying a fitness tracker (a second hand Garmin Forerunner 456Music) has greatly helped tracking my time and motivating me, and I got the habit of doing Yoga for 30min first thing in the morning after my 10min meditation, and running around 2-3 times a week (~7km/run).

Here are the apps I use:
- Yoga: [DownDog app](https://www.downdogapp.com)
- Meditation: [mindfulness.com](https://mindfulness.com)
- Fitness tracker: [Garmin connect](https://connect.garmin.com/). I am not too comfortable with using this since it sends all my data to Garmin's servers, and although they have a pretty decent privacy policy I might try to build my own program that reads the raw data of the fitness tracker I have.

_Update (23/04/2021)_: I just [ran my first half marathon](https://online.batavierenrace.nl/en/result/4456) during the 2021 Bataverien running race!
_Update (12/09/2021)_: I just [ran my first 20km of Brussels](http://prod.chronorace.be/result/sibp/Classement20km.aspx?eventId=1190375955890433) in 1h44. 
<!-- https://www.instagram.com/p/CTwMh6yoLPK/ -->

### Reading
My initial goal was to read on average 30min/day. I don't know if I'll be able to keep up with this, as I'm very much behind. 

Books I've read so far (_updated 28/12_):
- [x] [Twilight of the money gods](https://www.goodreads.com/book/show/35659719-twilight-of-the-money-gods) : very interesting take on the history of economics.
- [x] Think Like an Anthropologist - Matthew Engelke : Anthropology is not for me... Did find stories of different cultures interesting.
- [x] [Logicomix](http://www.logicomix.com) : one of the best educational comic books I've read
- [x] [In Praise of Idleness](http://www.zpub.com/notes/idle.html) : very interesting take on modern work philosophy, truly ahead of his time. [alternative link](https://harpers.org/archive/1932/10/in-praise-of-idleness/)
- [ ] Optimism over dispair - Noam Chomsky : I won't finish this book, as it is simply a collection of interviews and is pretty repetitive.
- [x] [Weapons of Math Destruction](https://en.wikipedia.org/wiki/Weapons_of_Math_Destruction) : very interesting book, would recommend to other Data Scientists.
- [x] [A mathematician's apology](https://www.math.ualberta.ca/mss/misc/A%20Mathematician's%20Apology.pdf) : very much disagree with most of the opinions at the beginning of this book, but interesting to see someone else's opinion.
- [x] [Thinking, Fast and Slow](https://en.wikipedia.org/wiki/Thinking%2C_Fast_and_Slow)
- [x] Sapiens
- [x] 1984
- [ ] The art of Strategy

I have also read multiple articles online, including [The Worst Mistake in the History of the Human Race](http://www.ditext.com/diamond/mistake.html).

### Music sheets
Trying to read/practice 1 music sheet a month. This can just be a piano exercise or a silly song I want to be able to play properly at jams.
So far, I've read (unchecked are still in progress):
- [x] [Polyrithm Challenge - Jesus Molina](https://musescore.com/user/33968822/scores/6232913) (Keys)
- [x] [Mii Channel Theme](https://musescore.com/user/2830596/scores/5180264) (Keys)
- [ ] [Oscar Peterson - A Little Jazz Exercise (Jesus Molina)](https://musescore.com/user/24335356/scores/5935123) (Keys)
- [ ] [La vie en rose](https://www.songsterr.com/a/wsa/edith-piaf-guitar-logic-la-vie-en-rose-tab-s464099t0) (Guitar)
- [x] [It runs through me](https://www.youtube.com/watch?v=hC9jSAlccBE) (Guitar)
- [ ] [Tadow](https://www.youtube.com/watch?v=R3jbOqu7lO4) (Guitar)
- [ ] [Jacob Collier's blues](https://www.youtube.com/watch?v=8bRlSwuj01c)

### Monthly challenges
#### March - Cold showers
The challenge description was very simple: take exclusively fully cold showers. Turned out to be a very effective way to save water at first, since it highly reduced my shower time.
Why? The feeling is just great. It wakes up your body, and gives a great feeling of achievement afterwards. 

#### April - blogposts
Here we are! I decided that I will take this month to fill up this website with short blogposts. Don't expect too much, I am not too comfortable writing and don't have any experience in doing this. And that's exactly why I'm doing it as a challenge! I plan on writing about 1 post/week (so around 4/5 posts).

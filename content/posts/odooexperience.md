+++
title = "💼 A year at Odoo"
date = 2022-07-10
description="My experience working as a Software Developer at Odoo"

[taxonomies]
categories = ["Growth"]
tags = ["Work", "Odoo"]
+++
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
_This blog section of my website has been silent for a while. Although I doubt that anyone really missed it, I'm personally happy to come back to writing after so long._

# Working at Odoo
From September 2021 to the end of last month, I worked as a Software Developer at Odoo.

It was my first full-time job, and I was excited to experience how it would work for me. In this post, I will talk about my experience at this Belgian Software company, what I learned, and what I liked and disliked.

## Background
So let's start with some background: beginning of April 2021 I was working on my thesis at the university of Maastricht. I was mostly done writing it and was unsure as to what I should do after I graduate. Starting a master straight away felt a bit scary, as I didn't really know what I wanted to do. Becoming financially independent of my parents was also something I was looking forward to, and the professional life was intriguing me.

Many things were scaring me about starting to work, such as not being able to take my own time off, having to work under strict rules and getting exhausted for working only on one thing. Most of these fears turned out to be true.
But at the time I was very much up for challenges and decided to look for job opportunities.

### Job hunt
I had three phases when applying for jobs:
1. Looking for the perfect project I want to work on.
2. Looking for projects that I would be interested in.
3. Sending my CV to any company I know that doesn't go against my ethics too much.

It was in this last phase of applying for jobs, after never hearing back from a few companies, being rejected from most of them (although to be fair, I often sent applications to companies without open positions) and accidentally applying for a "Forklift driver" position at a Dutch company, that I applied at Odoo.

<div class="chart-container" style="position: relative; width: 20vw; height: 20vw;">
    <canvas id="rejectionChart"></canvas>
</div>
<script>
    const data = {
        labels: [
            'No Answer',
            'Rejected',
            'Interview',
            'Job offer'
        ],
        datasets: [{
            label: 'Job Applications',
            data: [3, 5, 3, 2],
            backgroundColor: [
            // grey
            'rgba(0, 0, 0, 0.1)',
            // red
            'rgba(255, 0, 0, 0.2)',
            // light green
            'rgba(0, 255, 0, 0.3)',
            // green
            'rgba(0, 255, 200, 0.7)'
            ],
            hoverOffset: 4
        }]
    };
    const config = {
        type: 'pie',
        data: data,
    };
    const myChart = new Chart(
        document.getElementById('rejectionChart'),
        config
    );
</script>

_Note that the above graph only shows the companies that rejected me. I sometimes applied to multiple job positions within the same company, and didn't get accepted for any of them._

Out of the three companies that I had an interview with, two of them offered me a contract, one of them being Odoo and the other a multinational in consulting. The third was a startup that only replied to me in July, a month after I received the other two contracts.

### Why I chose Odoo
One of the things that differentiated Odoo from the other two companies was its job recruitment process. HR was very kind, responsive and helpful. Everything went smoothly and everyone seemed to have a clue of what they were doing.

Like the other multinational company, the interview process consisted of two main steps: an interview with HR to make sure I could apply for the job from a broader point of view, and a technical interview. The technical interview was preceded by an online programming test from hackerrank.com, and a IQ-like test.
The technical interview was with an Odoo developer and lasted 3-4 hours (!!), during which I was given more programming exercises and was asked to solve them thinking out loud. I was later told that only around 1% of applicants are sent a contract ([source](https://www.lecho.be/dossiers/emploi/comment-odoo-s-y-prend-il-pour-engager-a-tour-de-bras/10330005.html)), and only around 15% make it past the technical interview.

What made me choose Odoo over the other company is the fact that throughout their interview process I felt like I learned something, and would have liked to work with the people that I had contact with. The other company was a lot colder, and their "technical" interview lasted only about 45 minutes during which a bored employee asked me questions like "What are the drawbacks of agile development?". I did not want to work in a company where developers are given a contract without being asked to write a single line of code.

### What is Odoo?
_I am no longer affiliated with Odoo, and none of the links below are referral links_

I only really learned what Odoo was 1 week into my training. As a kid/teenager, I saw multiple Odoo cars in my neighbourhood with the slogan "Open Source Business Apps", which is how I got to know it first and decided to apply there (I was more interested in the words "Open Source" than "Business").
The first two weeks of work at Odoo are of functional training. Developers, Business Analysts and Sales recruits all do the same training to learn what Odoo is and how it works, hands on.

Odoo's goal is to provide software for primarily small/medium companies, for all their business needs. It offers an app for everything a company would need, from accounting, website builder, project manager, point of sales... All apps are connected to the same database and communicate with each other, meaning that a sale on your website will automatically be tracked by your accounting application.

Odoo claims to be much cheaper and more user friendly than its alternatives, especially the ERP (Enterprise Resource Planning) software SAP.
#### How do they make money?
[Odoo's code](https://github.com/odoo/odoo) is only partly open source. It includes the core of Odoo and most of, but not all, Odoo's modules (module = app), and has a slightly different look. The "Accounting" app, for example, is closed source. This version of the code is called the "Community Edition". The "Enterprise Edition" is built on top of the Community Edition, and adds some other apps and functionalities.

Anyone can run the Community Edition from their own laptop/server freely, and develop their own modules. The Enterprise Edition has to be paid monthly, and can be installed on your own server or with Odoo SaaS (Software as a Service). Anyone can make a [free Odoo trial](http://odoo.com/trial), and use a free app.

Additional fees also have to be paid for optional services such as sending SMS from Odoo or searching a directory of all registered companies. This is called Odoo IAP.

#### About the product itself
What do I think of the product itself? I think it has a lot of potential, and would recommend it to small business that need an ERP to handle their company. In fact, we set up the Community Edition on a server for the [Homehaven](https://homehaven.nl) project I'm involved in, and I'm happy to continue using it.

Odoo isn't perfect, tho. There can be a lot of frustrating error messages appearing that are impossible to understand and debug for a simple user, and bigger companies should also be concerned about its security.
Throughout my stay a colleague of mine found 3 serious security issues, sometimes allowing some users to upgrade themselves to administrators, and much worse.

Most of them have been fixed, but I can unofficially say that I believe that at list one serious security issue still exists. Up until now, Odoo hasn't taken security as a main priority, and has been mostly lucky that its codebase is so big and complex that not many people know where to start.

## Working at Odoo
Let's start with some information about my work:
- Unlike Sales and Business Analysts that work in fancy (and noisy) buldings with a rooftop, Odoo Developers work in a quiet farm in the middle of nowhere in Belgium
- Every developer is assigned to a sub-team focusing on a different part of Odoo. I chose to be in the "Social and Marketing" squad, focusing on apps such as CRM, eLearning and Mass Mailing.
- For most of my time at Odoo, developers only had to go to the farm 2 days a week (Thursday and Friday for my team), and it was pretty relaxed. This seems to have changed since I left, with rules being much stricter

### My usual day at Odoo
My work day would start by writing a "Hello 👋" message on my teams' Discord channel (despite having written its own Discord clone "Discuss", Odoo still used Discord for their internal communication). I would then shortly check my messages and emails to see if there are some updates from my tasks. I would otherwise open the "Project" app on Odoo, see if I got feedback from testing my features or validating my code and otherwise continue working on my task. If I wouldn't have any feedback, I would look for new tasks, and dive deep into the undocumented Odoo code trying to look for the problem/where to add a new feature.

Odoo is almost entirely written in Python and JavaScript. Most developers I worked with (including myself) preferred working with Python rather than JavaScript, also because Odoo's JavaScript framework called Owl is still under construction and can sometimes be difficult to understand. 
I spent the first few months setting up my environment to optimize my workflow, which turned out to save me a lot of time. For example, I found a very simple browser add-on that logs into the database automatically instead of having to manually write "admin":"admin" every time, which is what most developers did.
One main problem with Odoo's code is that it is almost entirely undocumented, so when asking "where's the documentation for this function?" the answer would always be "just look for examples in the code". Surprisingly this didn't turn out to be a big problem, as I learned to navigate and search the code really quickly. 

My colleagues saw me as someone who works very quickly (I usually had around 8 open tasks at the same time), always being able to find a solution for my problems even if sometimes overlooking some things.

Every employee got a 4 letter "quadrigram" (with old employees having a "trigram"), mine being faba. This was used to easily find, remember and communicate with others. 

I really enjoyed working in my team. The Product Owner, Luc Nailis, was a very positive and open person. Everyone else was very competent and willing to discuss and help each other for any tasks, without hesitating to say when they thought something was stupid. I really enjoyed this open criticism, although some people worried that newcomers wouldn't understand their good intentions. I once got a message from an older Odoo employee on my pull request that my change related to time zones was "stupid" (it was, I learned to [never deal with time zones since then](https://www.youtube.com/watch?v=-5wpm-gesOY)), and got some chocolate on my desk (with a post it with the number of the pull request and no other text) the next day as an "excuse".
Our discord chat would always be full of silly jokes and memes, which I never felt impacted our productivity but rather lifted the mood.

While coding, I'd often listen to music albums. I listen to over 300 full albums while working.
#### Home office
Not having set an alarm, I usually woke up at around 8-9am. I then took my work laptop, and started my day as described above.
I worked from multiple places, such as my family's place in Brussels and my room in Ottignies. 
#### At the Farm
When leaving from Brussels, a colleague would drive me from there to the farm. We would listen to his same 10-20 songs every time (YouTube Music's algorithm always recommended them in his daily mix). We'd sometimes discuss about books we read, and tasks at work.

### Developing an Odoo app from scratch
One of Odoo's internal reputation is that they like to re-build existing apps from scratch and integrate it into their ecosystem (one notable example is a version of Excel/Google Sheet online completely written from scratch). After a few months working at Odoo, we had a team Roadmap in which they revealed that Odoo will be working on a copy of Notion, called "Knowledge". I took part of the sub-sub-team responsible to write it from scratch with 3 other programmers, one of them being an experienced programmer.

How did it go? Surprisingly well. We did manage to reach our goals, and forged a really nice team spirit. The app is already realeased in an intermediate version and will be a key point of the next major release. As we then highlighted in the end-of-project meeting, we should have perhaps been a bit clearer with some of the specifications from the starts (that were made by the Product Owner in Figma). In particular, I ended up writing the tests for security rules from scratch more than 3 times, as we fundamentally changed how they behaved.

Despite not officially being "AGILE" (we didn't even have a morning meeting every day, which I liked), we communicated non-stop (the Product Owner being active part of the discussion) and were constantly updated with what the others were doing. This project made me learn an incredible number of tasks, from communication to security, and I am very happy to have been part of it.

### Company Culture
Odoo is known to have a strong company culture. They want to keep their "startup environment", with a more-or-less flat hierarchy and a very strong trust among colleagues and general friendliness.

One example of this is the fact that when talking to anyone in french (including the CEO), we used the informal "tu". This is a very big deal for the French language and culture, as everyone would expect you to use the much colder "vous" at work. The CEO, CFO and CTO also don't want to be seen from the top, and regularly joke around and play table tennis with their employees.

Odoo's trust for employees is truly somethings admirable. Anyone in the company could see how much the company made that month, and large amounts of free beer (yes, free beer) was laying around at the entrance employees could drink it after work (which I didn't get to enjoy, as I don't drink beer).
#### Things I liked
- Stress free environment. Although I did feel stressed for internal reasons, I never really felt stress coming from other colleagues or company deadlines. Everyone seemed to know what they're doing, and code is tested and reviewed multiple times before being merged in the `master` branch, and is further tested by all developers before going into production.
- Code being open-source. I'm really happy that other people can look at the code I wrote, and with machine learning models being trained on public open source projects I was very happy to make [a pull request to make masculine pronouns neutral](https://github.com/odoo/odoo/pull/91292).
- The hierarchy not being too rigid, and my team leader (or "guru") being a colleague first of all, working on the same tasks as us and willing to 
- Odoo employees also organized and took part in a lot of cool events. I went running twice the 20km of Brussels with Odoo, and 
- I also really liked their idea of building everything from scratch.
- Odoo's trust in young developers is also something to be showcased. I didn't have to timesheet myself, and taking breaks or holidays was never seen as a bad thing. This was also true when recruiting me, since I had relatively little work experience on my CV and they chose me solely based on my job interview.

#### Things I didn't like
Most of the things I disliked about Odoo were due to different ethical and philosophical ideas between me and Odoo's CEO (Fabien Pinckaers) and CTO (Antony Lesuisse). This includes:
- The non-negligible impact Odoo employers have on the environment
    - Almost every employee has a car, and travels alone to work. This is because the Odoo Farms are very far away from any big city, and going there by public transport is not an option
    - Eating Odoo's free lunch made by their chef is incredibly hard as a vegetarian, and nearly impossible as a vegan in order to maintain a healthy and balanced data. My guess is that around 80% of dishes contain meat, and 90% dairy products. Furthermore, food is certainly tasty but very fat and does not constitute a healthy diet.
    - During my time at Odoo, I have also seen a lot of food waste.
- The underlying idea that people have to spend most of their days/lives working, and that although Odoo helps automatizing repetitive tasks, it still makes people work more. I elaborate more on that below.

Other, unrelated issues:
- Odoo claims to have a lot of international employees. This is probably true for the Sales department and Business Analysts, but I could not find the same atmosphere within developers. Most developers are Belgian males, and coming from an international background I lacked this aspect of diversity.
##### Working from 9 to 5
Working hours at Odoo are fairly flexible for developers. No one really minds if you start at 7 or at 10 and finish earlier or later as long as you do your hours.
I have never worked extra hours and it is part of company culture to not require employees to work more than their contract hours.

The issue for me, however, is that sometimes I will have a very productive morning and will find myself in front of my laptop at 2pm knowing that I have to work but that my body and mind not only don't want to, but can't be productive. My eyes are tired, my body wants to move and my mind needs a break. I know I can't be productive, and yet I still have to be contactable and in front of my laptop.

I personally would prefer working by milestone, rather than by hour, with milestones as human as possible. I might write another blogpost about this another time, but as mentioned above I observed this idea that "humans _have_ to work", so we need to constantly find ways of optimising work so that they can work more.

## What I learned
Here are the main things I learned at Odoo:
- Deep knowledge for how to use git/to contribute to open source projects
- How to organize a team - how to set up efficient pipelines and talk to other developers and clients
- Navigating and understanding a large codebase
- Writing useful unit tests
- AGILE development